import Location from 'react-app-location';
import * as Yup from 'yup';

const idValidator = Yup.number().integer().required();

export const SecretariaLocation = new Location('/secretarias');
export const AddSecretariaLocation = new Location(`${SecretariaLocation.path}/adicionar`);
export const ViewSecretariaLocation = new Location(`${SecretariaLocation.path}/visualizar/:id`, {
    id: idValidator
});
export const EditSecretariaLocation = new Location(`${SecretariaLocation.path}/editar/:id`, {
    id: idValidator
});
export const RemoveSecretariaLocation = new Location(`${SecretariaLocation.path}/remover/:id`, {
    id: idValidator
});