import React from 'react';
import { Switch, Route } from 'react-router-dom';
import PrivateRoute from './PrivateRoute';
import Funcionarios from '../Components/Funcionarios';
import Secretarias from '../Components/Secretarias';
import Signin from '../Components/Signin';
import Usuarios from '../Components/Usuarios';
import * as secretariasLocations from './secretariasLocations';
import * as funcionariosLocation from './funcionariosLocations';
import * as usuariosLocations from './usuariosLocation';
import * as relatoriosLocations from './relatoriosLocation';
import { withAddSecretariaDialog } from '../Components/Secretarias/AddSecretariaDialog';
import { withEditSecretariaDialog } from '../Components/Secretarias/EditSecretariaDialog';
import { withViewSecretariaDialog } from '../Components/Secretarias/ViewSecretariaDialog';
import { withRemoveSecretariaDialog } from '../Components/Secretarias/RemoveSecretariaDialog';
import { withRemoveFuncionarioDialog } from '../Components/Funcionarios/RemoveFuncionarioDialog';
import { withViewFuncioarioDialog } from '../Components/Funcionarios/ViewFuncionarioDialog';
import { withEditFuncionarioDialog } from '../Components/Funcionarios/EditFuncionarioDialog';
import { withAddFuncionarioDialog } from '../Components/Funcionarios/AddFuncionarioDialog';
import { withAddUsuarioDialog } from '../Components/Usuarios/AddUsuarioDialog';
import { withEditUsuarioDialog } from '../Components/Usuarios/EditUsuarioDialog';
import { withViewUsuarioDialog } from '../Components/Usuarios/ViewUsuarioDialog';
import { withRemoveUsuarioDialog } from '../Components/Usuarios/RemoveUsuarioDialog';
import Relatorios from '../Components/Relatorios';
import withSalarioSecretariaReport from '../Components/Relatorios/SalarioSecretariaReport';
import withFuncionariosPeriodoAdmissaoExoneracaoReport from '../Components/Relatorios/FuncionariosPeriodoAdmissaoExoneracaoReport'
import * as rootLocation from './rootLocation';

const RouteSwitch = () => (
    <React.Fragment>
        <Switch>
            <PrivateRoute
                path={funcionariosLocation.RemoveFuncionarioLocation.path}
                component={withRemoveFuncionarioDialog(Funcionarios)}
            />
            <PrivateRoute
                path={funcionariosLocation.ViewFuncionarioLocation.path}
                component={withViewFuncioarioDialog(Funcionarios)}
            />
            <PrivateRoute
                path={funcionariosLocation.EditFuncionarioLocation.path}
                component={withEditFuncionarioDialog(Funcionarios)}
            />
            <PrivateRoute
                path={funcionariosLocation.AddFuncionarioLocation.path}
                component={withAddFuncionarioDialog(Funcionarios)}
            />
            <PrivateRoute
                exact
                path={funcionariosLocation.FuncionariosLocation.path}
                component={Funcionarios}
            />

            <PrivateRoute
                path={secretariasLocations.RemoveSecretariaLocation.path}
                component={withRemoveSecretariaDialog(Secretarias)}
            />
            <PrivateRoute
                path={secretariasLocations.ViewSecretariaLocation.path}
                component={withViewSecretariaDialog(Secretarias)}
            />
            <PrivateRoute
                path={secretariasLocations.EditSecretariaLocation.path}
                component={withEditSecretariaDialog(Secretarias)}
            />
            <PrivateRoute
                path={secretariasLocations.AddSecretariaLocation.path}
                component={withAddSecretariaDialog(Secretarias)}
            />
            <PrivateRoute
                exact
                path={secretariasLocations.SecretariaLocation.path}
                component={Secretarias}
            />
            <PrivateRoute
                path={usuariosLocations.RemoveUsuarioLocation.path}
                component={withRemoveUsuarioDialog(Usuarios)}
            />
            <PrivateRoute
                path={usuariosLocations.ViewUsuarioLocation.path}
                component={withViewUsuarioDialog(Usuarios)}
            />
            <PrivateRoute
                path={usuariosLocations.EditUsuarioLocation.path}
                component={withEditUsuarioDialog(Usuarios)}
             />
             <PrivateRoute
                path={usuariosLocations.AddUsuarioLocation.path}
                component={withAddUsuarioDialog(Usuarios)}
             />
             <PrivateRoute
                exact
                path={usuariosLocations.UsuariosLocation.path}
                component={Usuarios}
             />
             <PrivateRoute
                 path={relatoriosLocations.SalarioSecretariaReportLocation.path}
                 component={withSalarioSecretariaReport(Relatorios)}
             />
             <PrivateRoute
                path={relatoriosLocations.FuncionariosPeriodoAdmissaoExoneracaoReportLocation.path}
                component={withFuncionariosPeriodoAdmissaoExoneracaoReport(Relatorios)}
             />
            <Route path={rootLocation.RootLocation.path} component={Signin}/>
        </Switch>
    </React.Fragment>
);

export default RouteSwitch;