import Location from 'react-app-location';
import * as Yup from 'yup';

const idValidator = Yup.number().integer().required();

export const UsuariosLocation = new Location('/usuarios');
export const AddUsuarioLocation = new Location(`${UsuariosLocation.path}/adicionar`);
export const EditUsuarioLocation = new Location(`${UsuariosLocation.path}/editar/:id`, {
    id: idValidator
});
export const ViewUsuarioLocation = new Location(`${UsuariosLocation.path}/visualizar/:id`, {
    id: idValidator
});
export const RemoveUsuarioLocation = new Location(`${UsuariosLocation.path}/remover/:id`, {
    id: idValidator
});
