import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import * as fromReducer from '../reducer';
import * as rootLocation from './rootLocation';

const PrivateRoute = ({ component: Component, isAuthenticated, ...others }) => (
    <Route
        render={props => {
            return isAuthenticated ? (
                <Component {...props}/>
            ) : (
                <Redirect
                    to={{
                        pathname: rootLocation.RootLocation.toUrl(),
                        state: { unauthenticated: true }
                    }}
                />
            );
        }}
        {...others}
    />
);

const mapStateToProps = state => ({
    isAuthenticated: fromReducer.getIsAuthenticated(state)
});


export default connect(mapStateToProps)(PrivateRoute);