import Location from 'react-app-location';
import * as Yup from 'yup';

const idValidator = Yup.number().integer().required();

export const FuncionariosLocation = new Location('/funcionarios');
export const AddFuncionarioLocation = new Location(`${FuncionariosLocation.path}/adicionar`);
export const EditFuncionarioLocation = new Location(`${FuncionariosLocation.path}/editar/:id`, {
    id: idValidator
});
export const ViewFuncionarioLocation = new Location(`${FuncionariosLocation.path}/visualizar/:id`, {
    id: idValidator
});
export const RemoveFuncionarioLocation = new Location(`${FuncionariosLocation.path}/remover/:id`, {
    id: idValidator
});
