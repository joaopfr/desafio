import Location from 'react-app-location';

export const RelatoriosLocation = new Location('/relatorios');
export const SalarioSecretariaReportLocation = new Location(`${RelatoriosLocation.path}/salario-secretaria`);
export const FuncionariosPeriodoAdmissaoExoneracaoReportLocation = new Location(`${RelatoriosLocation.path}/funcionario-periodo`);