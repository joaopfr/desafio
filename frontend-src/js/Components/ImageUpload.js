import React from 'react';
import { connect } from 'react-redux';
import { saveAs } from 'file-saver';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import IconButton from '@material-ui/core/IconButton';
import ClearIcon from '@material-ui/icons/Clear';
import * as actions from '../actions';
import {
    getUser,
    getMediaDocumentation,
    getHasStartedMediaDocumentationDownload
} from '../reducer';

const styles = theme => ({
    fileInput: {
        display: 'none'
    },
    upload: {
        height: theme.typography.fontSize * 10,
        width: theme.typography.fontSize * 10,
        border: '2px dashed rgb(187, 186, 186)',
        borderRadius: '50%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
        fontSize: '16px',
        cursor: 'pointer',
        marginBottom: theme.spacing.unit
    },
    highlight: {
        backgroundColor: theme.palette.primary.light
    },
    icon: {
        opacity: 0.3,
        height: theme.typography.fontSize * 2,
        width: theme.typography.fontSize * 2
    },
    paper: {
        display: 'flex',
        justifyContent: 'space-between',
        marginBottom: theme.spacing.unit
    },
    fileNameTypography: {
        padding: theme.spacing.unit * 2,
        color: 'rgba(0, 0, 0, 0.54)'
    },
    errorTypography: {
        color: theme.palette.error.main
    }
});

const stylesHOC = withStyles(styles);

class ImageUploadedCard extends React.Component {
    constructor(props) {
        super(props);
        this.handleLinkOnClick = this.handleLinkOnClick.bind(this);
    }

    componentDidMount() {
        const {
            stopDownloadMediaDocumentation,
            hasStartedMediaDocumentationDownload,
            file,
            mediaDocumentation
        } = this.props;
        if (hasStartedMediaDocumentationDownload === true) {
            saveAs(mediaDocumentation, file.type, file.name);
            stopDownloadMediaDocumentation();
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const {
            stopDownloadMediaDocumentation,
            hasStartedMediaDocumentationDownload,
            file,
            mediaDocumentation
        } = this.props;
        if (prevProps.hasStartedMediaDocumentationDownload === false && hasStartedMediaDocumentationDownload === true) {
            if (file.uuid && mediaDocumentation.uuid === file.uuid) {
                const b = new Blob([mediaDocumentation.data]);
                saveAs(b, file.name, file.type);
                stopDownloadMediaDocumentation();
            }
        }
    }

    handleLinkOnClick(e) {
        e.preventDefault();

        const {
            user,
            file,
            startDownloadMediaDocumentation
        } = this.props;

        if (typeof(file.uuid) === 'undefined') {
            saveAs(file, file.type, file.name);
            return;
        }

        startDownloadMediaDocumentation(user)(file.uuid);
    }

    render() {
        const {
            classes,
            readOnly,
            file,
            onRemove
        } = this.props;
        return (
            <Paper elevation={1} className={classes.paper}>
                <Typography className={classes.fileNameTypography}>
                    <Link href={file.name} onClick={this.handleLinkOnClick}>
                        {file.name}
                    </Link>
                </Typography>
                <IconButton disabled={readOnly} onClick={() => readOnly ? null : onRemove(file)}>
                    <ClearIcon/>
                </IconButton>
            </Paper>
        );
    }
}

const mapStateToProps = state => ({
    user: getUser(state),
    hasStartedMediaDocumentationDownload: getHasStartedMediaDocumentationDownload(state),
    mediaDocumentation: getMediaDocumentation(state)
});
const mapDispatchToProps = dispatch => ({
    startDownloadMediaDocumentation(user) {
        return uuid =>
            dispatch(actions.startDownloadMediaDocumentation(uuid, user))
    },

    stopDownloadMediaDocumentation() {
        return dispatch(actions.stopDownloadMediaDocumentation())
    }
});

const connectHOC = connect(mapStateToProps, mapDispatchToProps);

const ImageUploadedCardWithStyle = stylesHOC(ImageUploadedCard);
const ImageUploadedCardWithConnect = connectHOC(ImageUploadedCardWithStyle);

const ImageUploadedCardList = ({ readOnly, files, onRemove }) => (
    <div>
        {
            files.map((f, index) => <ImageUploadedCardWithConnect
                key={`${f.name}${index}`}
                readOnly={readOnly}
                file={f}
                onRemove={onRemove}
            />)
        }
    </div>
);

class ImageUploadInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            highlight: false
        };

        this.onDragOver = this.onDragOver.bind(this);
        this.onDragLeave = this.onDragLeave.bind(this);
        this.onDrop = this.onDrop.bind(this);
    }

    onDragOver(e) {
        e.preventDefault();
        this.setState({
            highlight: true
        });
    }

    onDragLeave() {
        this.setState({
            highlight: false
        })
    }

    onDrop(e) {
        e.preventDefault();
        this.props.onFileSelect(e.dataTransfer.files);
        this.setState({
            highlight: false
        });
    }

    render() {
        const {classes,
            readOnly,
            onFileSelect
        } = this.props;

        let inputNode = null;
        return (
            <div
                className={`${classes.upload} ${this.state.highlight ? classes.highlight : ''}`}
                onClick={() => readOnly ? null : inputNode.click()}
                onDragOver={this.onDragOver}
                onDragLeave={this.onDragLeave}
                onDrop={this.onDrop}
            >
                <input
                    ref={inputDOM => inputNode = inputDOM}
                    multiple
                    id='fileUploadInput'
                    className={classes.fileInput}
                    type='file'
                    onChange={e => onFileSelect(e.currentTarget.files)}
                    accept='image/*'
                />
                <CloudUploadIcon className={classes.icon}/>
            </div>
        );
    };
}

const ImageUploadInputWithStyle = stylesHOC(ImageUploadInput);

const filterFiles = f => f.status === 'ADDED' || (typeof(f.uuid) !== 'undefined' && f.status !== 'REMOVED');
const ImageUpload = ({ classes, error, helperText, readOnly, files, onFileSelect, onFileRemove }) => (
    <React.Fragment>
        <Grid container justify='space-between'>
            {
                readOnly ? null :
                    <Grid item xs={12} sm={6}>
                        <ImageUploadInputWithStyle
                            readOnly={readOnly}
                            onFileSelect={onFileSelect}
                        />
                    </Grid>
            }
            {
                readOnly ?
                    <Grid item xs={12}>
                        <ImageUploadedCardList
                            readOnly={readOnly}
                            files={files}
                            onRemove={onFileRemove}
                        />
                    </Grid> :
                    <Grid item xs={12} sm={6}>
                        <ImageUploadedCardList
                            readOnly={readOnly}
                            files={files.filter(filterFiles)}
                            onRemove={onFileRemove}
                        />
                    </Grid>
            }
        </Grid>
        {
            error === true ?
                <Typography className={classes.errorTypography} variant='caption' gutterBottom>
                    {helperText}
                </Typography> :
                null
        }

    </React.Fragment>
);

export default stylesHOC(ImageUpload);