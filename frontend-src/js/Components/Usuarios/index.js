import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import Fab from '@material-ui/core/Fab';
import Grid from '@material-ui/core/Grid';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import AddIcon from '@material-ui/icons/Add';
import ErrorIcon from '@material-ui/icons/Error';
import NavegationBar from '../Navegation';
import EntityTable from '../EntityTable';
import * as actions from '../../actions';
import * as usuariosLocations from '../../routes/usuariosLocation';
import {
    getUsuarios,
    getIsLoading,
    getUser,
    getHasFetchingError,
    getError,
    getHasValidationErrors,
    getValidationEntity,
    getValidationErrors
} from '../../reducer';

const styles = theme => ({
    error: {
        backgroundColor: theme.palette.error.dark
    }
});

const header= ['Id', 'Email', 'Papel'];
const attrs = ['id', 'email', 'roles'];

const locations = {
    ViewLocation: usuariosLocations.ViewUsuarioLocation,
    EditLocation: usuariosLocations.EditUsuarioLocation,
    RemoveLocation: usuariosLocations.RemoveUsuarioLocation
};

class Usuarios extends React.Component {
    constructor(props) {
        super(props);

        this.handleSnackClose = this.handleSnackClose.bind(this);

        this.state = {
            showFetchErrorSnack: true
        };
    }

    componentDidMount() {
        const {
            match,
            user,
            fetchUsuarios
        } = this.props;

        if (match.path === usuariosLocations.UsuariosLocation.toUrl()) {
            fetchUsuarios(user)();
        }
    }

    handleSnackClose() {
        this.setState({
            showFetchErrorSnack : false
        });
    }

    render() {
        const {
            classes,
            usuarios,
            user,
            isLoading,
            CrudDialog = () => null,
            history,
            match,
            error,
            hasFetchingError,
            hasValidationErrors,
            validationUsuario,
            validationErrors,
            onAddUsuario,
            onEditUsuario,
            onRemoveUsuario
        } = this.props;

        const onClose = () => history.push(
            usuariosLocations.UsuariosLocation.toUrl()
        );

        const usuario = typeof (match.params.id) === 'undefined' ? null : usuarios.filter(
            u => u.id === parseInt(match.params.id)
        )[0];

        const onDialogActions = {
            onAddUsuario: onAddUsuario(user),
            onEditUsuario: onEditUsuario(user),
            onRemoveUsuario: onRemoveUsuario(user)
        };

        return (
            <React.Fragment>
                <NavegationBar/>
                <EntityTable
                    entityLocations={locations}
                    entityHeader={header}
                    entityAttrs={attrs}
                    entityData={usuarios}
                />

                {isLoading ? <CircularProgress size={100}/> : null}

                <Grid container justify='flex-end'>
                    <Link to={usuariosLocations.AddUsuarioLocation.toUrl()}>
                        <Fab color='primary'>
                            <AddIcon/>
                        </Fab>
                    </Link>
                </Grid>

                <CrudDialog
                    open={true}
                    onClose={onClose}
                    usuario={usuario}
                    isLoading={isLoading}
                    hasValidationErrors={hasValidationErrors}
                    validationUsuario={validationUsuario}
                    validationErrors={validationErrors}
                    {...onDialogActions}
                />

                <Snackbar
                    anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'center'
                    }}
                    autoHideDuration={5000}
                    open={this.props.showFetchErrorSnack && hasFetchingError}
                    onClose={this.handleSnackClose}
                >
                    <SnackbarContent
                        className={classes.error}
                        message={
                            <span>
                            <ErrorIcon/>
                                {error}
                        </span>
                        }
                    />
                </Snackbar>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    usuarios: getUsuarios(state),
    user: getUser(state),
    isLoading: getIsLoading(state),
    hasFetchingError: getHasFetchingError(state),
    error: getError(state),
    hasValidationErrors: getHasValidationErrors(state),
    validationUsuario: getValidationEntity(state),
    validationErrors: getValidationErrors(state)
});

const mapDispatchToProps = dispatch => ({
    fetchUsuarios(user) {
        return () => dispatch(
            actions.fetchUsuarios(user)
        );
    },

    onAddUsuario(user) {
        return novoUsuario => dispatch(
            actions.addUsuario(novoUsuario, user)
        );
    },

    onEditUsuario(user) {
        return usuario => dispatch(
            actions.editUsuario(usuario, user)
        );
    },

    onRemoveUsuario(user) {
        return id => dispatch(
            actions.removeUsuario(id, user)
        );
    },

    clearValidationErrors() {
        return dispatch(actions.clearValidationErrors());
    }
});

const componentStyles = withStyles(styles);
const componentReduxProps = connect(
    mapStateToProps,
    mapDispatchToProps
);

export default componentReduxProps(componentStyles(Usuarios));
