import React from 'react';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import UsuarioForm from './UsuarioForm';

export const EditUsuarioDialog = props => {
    const {
        usuario,
        open,
        onClose,
        onEditUsuario,
        isLoading,
        hasValidationErrors,
        validationUsuario,
        validationErrors
    } = props;

    const formNode = React.createRef();
    const onSubmit = usuario => onEditUsuario(usuario).then(
        () => onClose()
    );

    return (
        <Dialog
            open={open}
            onClose={onClose}
        >
            <DialogTitle>Editar Usuário</DialogTitle>
            <DialogContent>
                <UsuarioForm
                    ref={formNode}
                    isAdd={false}
                    isReadOnly={false}
                    onSubmit={onSubmit}
                    usuario={usuario}
                    hasValidationErrors={hasValidationErrors}
                    validationUsuario={validationUsuario}
                    validationErrors={validationErrors}
                />
            </DialogContent>

            {isLoading ? <CircularProgress size={100}/> : null}

            <DialogActions>
                <Button variant='outlined' color='primary' onClick={onClose}>
                    Cancelar
                </Button>
                <Button variant='contained' color='primary' onClick={() =>
                    formNode.current.submitForm()}
                >
                    Editar
                </Button>
            </DialogActions>

        </Dialog>
    );
};

export const withEditUsuarioDialog = Usuario => props =>
    <Usuario CrudDialog={EditUsuarioDialog} {...props}/>;
