import React from 'react';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import UsuarioForm from './UsuarioForm';

export const AddUsuarioDialog = props => {
    const {
        open,
        onClose,
        onAddUsuario,
        isLoading,
        hasValidationErrors,
        validationUsuario,
        validationErrors
    } = props;

    const formNode = React.createRef();
    const onSubmit = novoUsuario => onAddUsuario(novoUsuario).then(
        () => onClose()
    );

    return (
        <Dialog
            open={open}
            onClose={onClose}
        >
            <DialogTitle>Adicionar Usuario</DialogTitle>
            <DialogContent>
                <UsuarioForm
                    ref={formNode}
                    isAdd={true}
                    isReadOnly={false}
                    onSubmit={onSubmit}
                    hasValidationErrors={hasValidationErrors}
                    validationUsuario={validationUsuario}
                    validationErrors={validationErrors}
                />
            </DialogContent>

            {isLoading ? <CircularProgress size={100}/> : null}

            <DialogActions>
                <Button variant='outlined' color='primary' onClick={onClose}>
                    Cancelar
                </Button>
                <Button variant='contained' color='primary' onClick={
                    () => formNode.current.submitForm()
                }>
                    Adicionar
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export const withAddUsuarioDialog = Usuarios => props => <Usuarios CrudDialog={AddUsuarioDialog} {...props}/>;
