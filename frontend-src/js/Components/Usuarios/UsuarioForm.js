import React from 'react';
import { Formik, Form, Field } from 'formik';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import Grid from '@material-ui/core/Grid';
import RadioGroup from "@material-ui/core/es/RadioGroup/RadioGroup";
import Radio from '@material-ui/core/Radio';
import TextField from '@material-ui/core/TextField';
import { mapErrors, ROLE_GERENTE, ROLE_OPERADOR, ROLE_ADMINISTRADOR } from '../utils';

const usuarioInitialValues = {
    id: '',
    email: '',
    password: '',
    roles: 'ROLE_ADMINISTRADOR'
};

class UsuarioFormikComponent extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        if (this.props.isInitialValid === false) {
            this.props.setErrors(this.props.validationErrors);
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.hasValidationErrors === false && this.props.hasValidationErrors === true) {
            this.props.setErrors(this.props.validationErrors);
        }
    }

    render() {
        const {
            values,
            handleSubmit,
            isReadOnly,
            isAdd,
            errors
        } = this.props;

        const readOnly = isReadOnly;
        const textFieldErrors = mapErrors(errors);

        return (
            <Form>
                <Grid container spacing={16} justify='flex-start'>
                    {
                        isAdd ?
                            null :
                            <Grid item xs={12} sm={6}>
                                <Field name='id' render={({ field }) => (
                                    <TextField label='Id' margin='normal' InputProps={{readOnly: true}} {...field}/>
                                )}/>
                            </Grid>
                    }

                    <Grid item xs={12} sm={6}>
                        <Field name='email' render={({ field }) => (
                            <TextField
                                label='Email'
                                margin='normal'
                                {...textFieldErrors.email}
                                InputProps={{ readOnly }}
                                {...field}/>
                        )}/>
                    </Grid>

                    {
                        readOnly ?
                            null :
                            <Grid item xs={12} sm={6}>
                                <Field name='password' render={({field}) => (
                                    <TextField
                                        label='Senha'
                                        margin='normal'
                                        type='password'
                                        autoComplete="current-password"
                                        {...textFieldErrors.password}
                                        {...field}
                                    />
                                )}/>
                            </Grid>
                    }

                    <Grid item xs={12}>
                        <Field name='roles' render={({ field }) => (
                            <FormControl fullWidth component='fieldset'>
                                <FormLabel component='legend'>Permissões</FormLabel>
                                <RadioGroup {...field}>
                                    <FormControlLabel
                                        value={ROLE_ADMINISTRADOR}
                                        control={<Radio color='primary' disabled={readOnly}/>}
                                        label='Administrador'
                                    />
                                    <FormControlLabel
                                        value={ROLE_GERENTE}
                                        control={<Radio color='primary' disabled={readOnly}/>}
                                        label='Gerente'
                                    />
                                    <FormControlLabel
                                        value={ROLE_OPERADOR}
                                        control={<Radio color='primary' disabled={readOnly}/>}
                                        label='Operador'
                                    />
                                </RadioGroup>
                            </FormControl>
                        )}/>
                    </Grid>
                </Grid>
            </Form>
        );
    }
}

const UsuarioForm = React.forwardRef((props, ref) => {
    const {
        usuario,
        onSubmit,
        hasValidationErrors,
        validationUsuario,
        ...otherProps
    } = props;

    const initialValues = (hasValidationErrors === true && validationUsuario) || usuario || usuarioInitialValues;
    initialValues.password = usuarioInitialValues.password;

    return (
        <Formik
            ref={ref}
            initialValues={initialValues}
            onSubmit={onSubmit}
            isinitialValid={hasValidationErrors === true}
            render={formikProps =>
                <UsuarioFormikComponent
                    hasValidationErrors={hasValidationErrors}
                    validationUsuario={validationUsuario}
                    {...formikProps}
                    {...otherProps}
                />
            }
        />
    );
});

export default UsuarioForm;