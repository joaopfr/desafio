import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';

const Transition = props => <Slide direction='up' {...props}/>;

export const RemoveUsuarioDialog = props => {
    const {
        usuario,
        open,
        onClose,
        onRemoveUsuario
} = props;
    return (
        <Dialog open={open} onClose={onClose} TransitionComponent={Transition}>
            <DialogTitle>Remover Usuário</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    {`Você confirma a remoção da do Usuário com email ${usuario.email} com Id ${usuario.id}?`}
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button variant='outlined' color='primary' onClick={onClose}>
                    Cancelar
                </Button>
                <Button variant='contained' color='primary' onClick={() => {
                    onRemoveUsuario(usuario.id);
                    onClose();
                }}>
                    Remover
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export const withRemoveUsuarioDialog = Usuarios => props =>
    <Usuarios CrudDialog={RemoveUsuarioDialog} {...props}/>;
