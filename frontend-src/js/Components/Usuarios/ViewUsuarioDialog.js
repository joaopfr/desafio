import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import UsuarioForm from './UsuarioForm';

export const ViewUsuarioDialog = props => {
    const {
        usuario,
        open,
        onClose
    } = props;

    return (
        <Dialog open={open} onClose={onClose}>
            <DialogTitle>Visualizar Usuário</DialogTitle>
            <DialogContent>
                <UsuarioForm
                    isAdd={false}
                    isReadOnly={true}
                    usuario={usuario}
                />
            </DialogContent>
            <DialogActions>
                <Button variant='contained' color='primary' onClick={onClose}>
                    Fechar
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export const withViewUsuarioDialog = Usuario => props =>
    <Usuario CrudDialog={ViewUsuarioDialog} {...props}/>;
