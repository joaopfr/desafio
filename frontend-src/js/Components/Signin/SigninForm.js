import React from 'react';
import { Formik, Form, Field } from 'formik';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

const styles = theme => ({
    button: {
        marginTop: theme.spacing.unit * 4
    }
});

const initialValues = {
    email: '',
    senha: ''
};

const SigninForm = ({ classes, onSubmit, hasFetchingError }) => (
    <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        render={formikProps => {
            return (
                <Form>
                    <Field name='email' render={({ field }) =>
                        <TextField
                            error={hasFetchingError}
                            fullWidth
                            required
                            label='Email'
                            margin='normal'
                            {...field}
                        />
                    }/>

                    <Field name='senha' render={({ field }) =>
                        <TextField
                            error={hasFetchingError}
                            fullWidth
                            label='Senha'
                            type='password'
                            margin='normal'
                            {...field}
                        />
                    }/>

                    <Button type='submit' fullWidth variant='contained' color='primary' className={classes.button}>
                        Entrar
                    </Button>
                </Form>
            );
        }}
    />
);

export default withStyles(styles)(SigninForm);