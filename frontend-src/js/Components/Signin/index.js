import React from 'react';
import { connect }from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import amber from '@material-ui/core/colors/amber';
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import AccountBoxIcon from '@material-ui/icons/AccountBox';
import SigninForm from './SigninForm';
import * as actions from '../../actions';
import * as funcionariosLocations from '../../routes/funcionariosLocations';
import * as fromReducer from '../../reducer';

const styles =  theme => {
    console.log('theme', theme);
    return ({
        paper: {
            paddingTop: theme.spacing.unit * 6,
            paddingBottom: theme.spacing.unit * 6,
            paddingLeft: theme.spacing.unit * 6,
            paddingRight: theme.spacing.unit * 6,
        },
        avatar: {
            color: '#fff',
            width: theme.spacing.unit * 10,
            height: theme.spacing.unit * 10,
            backgroundColor: theme.palette.primary.dark
        },
        error: {
            backgroundColor: theme.palette.error.dark
        },
        unauthenticated: {
            backgroundColor: amber[700]
        }
    });
};

class AuthErrorSnackbar extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            shouldClose: false
        };

        this.handleOnClose = this.handleOnClose.bind(this);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.hasFetchingError === false && this.props.hasFetchingError === true) {
            this.setState({shouldClose: false});
        }
    }

    handleOnClose(event, reason) {
        if (reason === 'clickaway') {
            return;
        }

        this.setState({
            shouldClose: true
        });
    }

    render() {
        return (
            <Snackbar
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'center'
                }}
                autoHideDuration={6000}
                open={this.props.hasFetchingError && !this.state.shouldClose}
                onClose={this.handleOnClose}
            >
                <SnackbarContent
                    className={this.props.classes.error}
                    message={<span>{this.props.error}</span>}
                />
            </Snackbar>
        )
    }
}

const AuthErrorSnackbarStyled = withStyles(styles)(AuthErrorSnackbar);

class MustSigninSnackbar extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            shouldClose: false
        };

        this.handleOnClose = this.handleOnClose.bind(this);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.unauthenticated === false && this.props.unauthenticated === true) {
            this.setState({shouldClose: false});
        }
    }

    handleOnClose(event, reason) {
        if (reason === 'clickaway') {
            return;
        }

        this.setState({
            shouldClose: true
        });
    }

    render() {
        return (
            <Snackbar
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'center'
                }}
                autoHideDuration={6000}
                open={this.props.unauthenticated && !this.state.shouldClose}
                onClose={this.handleOnClose}
            >
                <SnackbarContent
                    className={this.props.classes.unauthenticated}
                    message={<span>É necessário estar logado para acessar o recurso desejado</span>}
                />
            </Snackbar>
        )
    }
}

const MustSigninSnackbarStyled = withStyles(styles)(MustSigninSnackbar);

const Signin = ({
        classes,
        onSigninSubmit,
        history,
        location,
        hasFetchingError,
        error
    }) => (
    <Grid container alignItems='center' justify='center' spacing={24}>
        <AuthErrorSnackbarStyled hasFetchingError={hasFetchingError} error={error}/>
        <MustSigninSnackbarStyled unauthenticated={location.state && location.state.unauthenticated === true}/>
        <Paper elevation={1} className={classes.paper}>
            <Grid container alignItems='center' direction='column' justify='center' spacing={16}>
                <Avatar className={classes.avatar}>
                    <AccountBoxIcon/>
                </Avatar>
                <SigninForm
                    hasFetchingError={hasFetchingError}
                    onSubmit={
                        user => onSigninSubmit(user).then(
                            () => history.push(
                                funcionariosLocations.FuncionariosLocation.toUrl()
                            )
                        )
                    }
                />
            </Grid>
        </Paper>
    </Grid>
);

const mapStateToProps = state => ({
    hasFetchingError: fromReducer.getHasFetchingError(state),
    error: fromReducer.getError(state)
});

const mapDispatchToProps = dispatch => ({
    onSigninSubmit(user) {
        return dispatch(
            actions.userSignin(user)
        );
    }
});

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps
);

export default withConnect(withStyles(styles)(Signin));