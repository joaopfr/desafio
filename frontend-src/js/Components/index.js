import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import RouteSwitch from '../routes/RouteSwitch';


const App = () => (
    <React.Fragment>
        <BrowserRouter>
            <RouteSwitch/>
        </BrowserRouter>
    </React.Fragment>
);

export default App;