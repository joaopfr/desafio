import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import TableCell from "@material-ui/core/TableCell";
import EntityRow from "./EntityRow";
import TableRow from "@material-ui/core/TableRow";



const EntityTable = ({ entityLocations, entityHeader, entityAttrs, entityData }) => (
    <Table>
        <TableHead>
            <TableRow>
                {entityHeader.map(h => (
                    <TableCell key={h}>
                        {h}
                    </TableCell>
                ))}
                <TableCell>
                    Ações
                </TableCell>
            </TableRow>
        </TableHead>
        <TableBody>
            {entityData.map(entity => (
                <EntityRow key={entity.id}
                           entityLocations={entityLocations}
                           entityAttrs={entityAttrs}
                           entity={entity}
                />
             ))}
        </TableBody>
    </Table>
);

export default EntityTable;