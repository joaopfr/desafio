import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';

const styles = {
    subMenuItem: {
        display: 'flex',
        justifyContent: 'space-between'
    }
};

class SubMenuItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            menuOpen: false,
            anchorEl: null
        };

        this.handleItemClick = this.handleItemClick.bind(this);
        this.handleSubMenuClose = this.handleSubMenuClose.bind(this);
    }

    handleItemClick(event) {
        const anchorEl = this.state.anchorEl === null ? event.currentTarget : this.state.anchorEl;

        this.setState({
            anchorEl,
            menuOpen: !this.state.menuOpen
        });
    }

    handleSubMenuClose() {
        this.setState({
            menuOpen: false
        });
    }

    render() {
        const {
            caption,
            menuItems
        } = this.props;

        return (
            <React.Fragment>
                <MenuItem
                    onClick={this.handleItemClick}
                >
                    {caption}
                    <ArrowRightIcon />
                </MenuItem>
                <Menu
                    open={this.state.menuOpen}
                    anchorEl={this.state.anchorEl}
                    onClose={this.handleSubMenuClose}
                    anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                    transformOrigin={{ vertical: 'top', horizontal: 'left' }}
                >
                    {menuItems}
                </Menu>
            </React.Fragment>
        );
    }
}

export default withStyles(styles)(SubMenuItem);
