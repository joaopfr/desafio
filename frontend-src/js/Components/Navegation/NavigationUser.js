import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import AccountCircle from '@material-ui/icons/AccountCircle';
import { clearRemoteRequestErrors, userLogout } from '../../actions';
import * as fromReducer from '../../reducer';
import * as rootLocation from '../../routes/rootLocation';
import grey from "@material-ui/core/colors/grey";

const styles = theme => ({
    icon: {
        color: grey[200]
    }
});

const stylesHOC = withStyles(styles);

class NavigationUser extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            anchorEl: null
        };

        this.handleClose = this.handleClose.bind(this);
        this.handleClickIconButton = this.handleClickIconButton.bind(this);
        this.handleLogout = this.handleLogout.bind(this);
    }

    handleClickIconButton(event) {
        this.setState({ anchorEl: event.currentTarget })
    };

    handleClose() {
        this.setState({ anchorEl: null })
    };

    handleLogout() {
        this.props.onLogout();
    };

    render() {
        const { user, classes } = this.props;
        const { anchorEl } = this.state;

        return (
            <IconButton onClick={this.handleClickIconButton}>
                <AccountCircle className={classes.icon}/>
                <Menu
                    anchorEl={anchorEl}
                    open={Boolean(anchorEl)}
                    onClose={this.handleClose}
                >
                    <MenuItem disabled>
                        Usuário: {user.email}
                    </MenuItem>
                    <MenuItem onClick={this.handleLogout}>
                        Logout
                    </MenuItem>
                </Menu>
            </IconButton>
        );
    }
}

const mapStateToProps = state => ({
    user: fromReducer.getUser(state)
});

const mapDispatchToProps = (dispatch, { history }) => ({
    onLogout() {
        return dispatch(userLogout()).then(
            dispatch(clearRemoteRequestErrors())
        ).then(
            history.push(rootLocation.RootLocation.toUrl())
        );
    }
});

const withReactRedux = connect(
    mapStateToProps,
    mapDispatchToProps
);

export default withRouter(
    withReactRedux(stylesHOC(NavigationUser))
);