import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import grey from '@material-ui/core/colors/grey';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import * as funcionariosLocation from '../../routes/funcionariosLocations';
import * as secretariasLocation from '../../routes/secretariasLocations';
import * as usuariosLocation from '../../routes/usuariosLocation';
import * as relatoriosLocation from '../../routes/relatoriosLocation';
import { isGranted } from '../utils';

const styles = theme => ({
    button: {
        color: grey[200]
    }
});

const stylesHOC = withStyles(styles);

const FuncionariosLink = props => <Link to={funcionariosLocation.FuncionariosLocation.toUrl()} {...props}/>;
const SecretariasLink = props => <Link to={secretariasLocation.SecretariaLocation.toUrl()} {...props}/>;
const UsuariosLink = props => <Link to={usuariosLocation.UsuariosLocation.toUrl()} {...props}/>;
const SalarioSecretariaReportLink = props => <Link to={relatoriosLocation.SalarioSecretariaReportLocation.toUrl()} {...props}/>;
const FuncionariosPeriodoAdmissaoExoneracaoReportLink = props => <Link to={relatoriosLocation.FuncionariosPeriodoAdmissaoExoneracaoReportLocation.toUrl()} {...props}/>;

class NavegationList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            relatoriosMenuAnchorEl: null,
            relatoriosMenuOpen: false
        };

        this.handleOnClickRelatoriosMenu = this.handleOnClickRelatoriosMenu.bind(this);
        this.handleCloseRelatoriosMenu = this.handleCloseRelatoriosMenu.bind(this);
    }

    handleOnClickRelatoriosMenu(event) {
        const anchorEl = this.state.anchorEl === null ? event.currentTarget : this.state.relatoriosMenuAnchorEl;
        this.setState({
            relatoriosMenuAnchorEl: anchorEl,
            relatoriosMenuOpen: !this.state.relatoriosMenuOpen
        });
    }

    handleCloseRelatoriosMenu() {
        this.setState({
            relatoriosMenuOpen: false
        });
    }

    render() {
        const { classes, user } = this.props;
        return (
            <Grid container spacing={16} style={{marginTop: '-2px'}} justify='flex-end'>
                {
                    isGranted(user.roles, funcionariosLocation.FuncionariosLocation) &&
                    <Grid item>
                        <Button className={classes.button} component={FuncionariosLink}>
                            Funcionários
                        </Button>
                    </Grid>

                }

                {
                    isGranted(user.roles, secretariasLocation.SecretariaLocation) &&
                    <Grid item>
                        <Button className={classes.button} component={SecretariasLink}>
                            Secretarias
                        </Button>
                    </Grid>
                }

                {
                    isGranted(user.roles, usuariosLocation.UsuariosLocation) &&
                    <Grid item>
                        <Button className={classes.button} component={UsuariosLink}>
                            Usuários
                        </Button>
                    </Grid>
                }

                {
                    isGranted(user.roles, relatoriosLocation.RelatoriosLocation) &&
                    <Grid item>
                        <Button className={classes.button} onClick={this.handleOnClickRelatoriosMenu}>
                            Relatórios
                            {
                                this.state.relatoriosMenuOpen ?
                                    <KeyboardArrowUpIcon/> :
                                    <KeyboardArrowDownIcon/>
                            }
                        </Button>
                        <Menu
                            anchorEl={this.state.relatoriosMenuAnchorEl}
                            open={this.state.relatoriosMenuOpen}
                            onClose={this.handleCloseRelatoriosMenu}
                            anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                            transformOrigin={{ vertical: 'bottom', horizontal: 'right' }}
                        >
                            <MenuItem onClick={this.handleOnClickRelatoriosMenu} component={FuncionariosPeriodoAdmissaoExoneracaoReportLink}>
                                Funcionários por período
                            </MenuItem>
                            <MenuItem onClick={this.handleOnClickRelatoriosMenu} component={SalarioSecretariaReportLink}>
                                Total do Salário Líquido
                            </MenuItem>
                        </Menu>
                    </Grid>
                }
            </Grid>
        );
    }
}

export default stylesHOC(NavegationList);