import React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import grey from '@material-ui/core/colors/grey';
import AppBar from '@material-ui/core/AppBar/AppBar';
import Grid from '@material-ui/core/Grid';
import Toolbar from '@material-ui/core/Toolbar/Toolbar';
import Typography from '@material-ui/core/Typography/Typography';
import Hidden from '@material-ui/core/Hidden';
import NavegationList from './NavegationList';
import NavegationMenu from './NavegationMenu';
import NavigationUser from './NavigationUser';
import * as fromReducer from '../../reducer';

const styles = theme => ({
    typography: {
        color: grey[200]
    },
    grid: {
        flexFlow: 'row'
    }
});

const Index = ({ classes, user }) => (
    <AppBar position='relative'>
        <Toolbar>
            <Hidden mdUp>
                <NavegationMenu user={user}/>
            </Hidden>
            <Typography className={classes.typography} variant='h4'>
                Desafio
            </Typography>
            <Grid className={classes.grid} container spacing={16} justify='flex-end'>
                <Hidden smDown>
                    <NavegationList user={user}/>
                </Hidden>
                <NavigationUser/>
            </Grid>
        </Toolbar>
    </AppBar>
);

const stylesHOC = withStyles(styles);

const IndexStyled = stylesHOC(Index);

const mapStateToProps = state => ({
    user: fromReducer.getUser(state)
});

export default connect(mapStateToProps)(IndexStyled);