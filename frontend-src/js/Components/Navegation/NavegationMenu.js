import React from 'react';
import { Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import grey from '@material-ui/core/colors/grey';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MenuIcon from '@material-ui/icons/Menu';
import * as funcionariosLocations from '../../routes/funcionariosLocations';
import * as secretariasLocations from '../../routes/secretariasLocations';
import * as usuariosLocation from '../../routes/usuariosLocation';
import * as relatoriosLocation from '../../routes/relatoriosLocation';
import { isGranted } from '../utils';
import SubMenuItem from './SubMenuItem';

const styles = theme => ({
    icon: {
        color: grey[200]
    }
});

const stylesHOC = withStyles(styles);

const FuncionariosLink = props => <Link to={funcionariosLocations.FuncionariosLocation.toUrl()} {...props}/>;
const SecretariasLink = props => <Link to={secretariasLocations.SecretariaLocation.toUrl()} {...props}/>;
const UsuariosLink = props => <Link to={usuariosLocation.UsuariosLocation.toUrl()} {...props}/>;
const SalarioSecretariaReportLink = props => <Link to={relatoriosLocation.SalarioSecretariaReportLocation.toUrl()} {...props}/>;
const FuncionariosPeriodoAdmissaoExoneracaoReportLink = props => <Link to={relatoriosLocation.FuncionariosPeriodoAdmissaoExoneracaoReportLocation.toUrl()} {...props}/>;

class NavegationMenu extends React.Component {
    constructor(props) {
        super(props);
        this.state = { anchorEl: null, open: false };
        this.handleOnClick = this.handleOnClick.bind(this);
        this.handleOnClose = this.handleOnClose.bind(this);
        this.handleMenuItemOnClick = this.handleMenuItemOnClick.bind(this);
    }

    handleOnClick(e) {
        this.setState({
            anchorEl: e.currentTarget,
            open: true
        });
    }

    handleOnClose() {
        this.setState({
            anchorEl: null,
            open: false
        });
    }

    handleMenuItemOnClick() {
        this.setState({
            open: false
        })
    }

    render() {
        const { classes, user } = this.props;

        const relatoriosMenuItems = [
            <MenuItem key='periodo' onClick={this.handleMenuItemOnClick} component={FuncionariosPeriodoAdmissaoExoneracaoReportLink}>
                Funcionários por período
            </MenuItem>,
            <MenuItem key='total' onClick={this.handleMenuItemOnClick} component={SalarioSecretariaReportLink}>
                Total do Salário Líquido
            </MenuItem>
        ];

        return (
            <React.Fragment>
                <IconButton onClick={this.handleOnClick}>
                    <MenuIcon className={classes.icon}/>
                </IconButton>
                <Menu
                    open={Boolean(this.state.open)}
                    anchorEl={this.state.anchorEl}
                    onClose={this.handleOnClose}
                >
                    {
                        isGranted(user.roles, funcionariosLocations.FuncionariosLocation) &&
                            <MenuItem onClick={this.handleMenuItemOnClick} component={FuncionariosLink}>
                                Funcionários
                            </MenuItem>
                    }

                    {
                        isGranted(user.roles, secretariasLocations.SecretariaLocation) &&
                            <MenuItem onClick={this.handleMenuItemOnClick} component={SecretariasLink}>
                                Secretarias
                            </MenuItem>
                    }

                    {
                        isGranted(user.roles, usuariosLocation.UsuariosLocation) &&
                            <MenuItem onClick={this.handleMenuItemOnClick} component={UsuariosLink}>
                                Usuários
                            </MenuItem>
                    }

                    {
                        isGranted(user.roles, relatoriosLocation.RelatoriosLocation) &&
                            <SubMenuItem
                                caption='Relatorios'
                                menuItems={relatoriosMenuItems}
                            />
                    }
                </Menu>
            </React.Fragment>
        );
    }
}

export default stylesHOC(NavegationMenu);