import React from "react";
import { Link } from 'react-router-dom';
import TableRow from '@material-ui/core/TableRow';
import TableCell from "@material-ui/core/TableCell";
import IconButton from "@material-ui/core/IconButton";
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import VisibilityIcon from '@material-ui/icons/Visibility';

const EntityRow = ({ entityLocations, entityAttrs, entity }) => (
    <TableRow>
        {entityAttrs.map(attr => (
            <TableCell key={attr}>
                {entity[attr]}
            </TableCell>
        ))}
        <TableCell>
            <Link
                to={entityLocations.ViewLocation.toUrl({
                    id: entity.id
                })}
            >
                <IconButton>
                    <VisibilityIcon/>
                </IconButton>
            </Link>
            <Link
                to={entityLocations.EditLocation.toUrl({
                    id: entity.id
                })}
            >
                <IconButton>
                    <EditIcon/>
                </IconButton>
            </Link>
            <Link
                to={entityLocations.RemoveLocation.toUrl({
                    id: entity.id
                })}
            >
                <IconButton>
                    <DeleteIcon/>
                </IconButton>
            </Link>
        </TableCell>
    </TableRow>
);
export default EntityRow;