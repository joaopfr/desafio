import { FuncionariosLocation } from '../routes/funcionariosLocations';
import { SecretariaLocation } from  '../routes/secretariasLocations';
import { UsuariosLocation } from '../routes/usuariosLocation';
import { RelatoriosLocation } from '../routes/relatoriosLocation';

export const mapErrors = errors => {
    const textFieldErrors = Object.keys(errors).reduce((newErrors, errorKey) => {
        newErrors[errorKey] = {
            error: typeof(errors[errorKey]) !== 'undefined' && errors[errorKey].length > 0,
            helperText: errors[errorKey] && errors[errorKey].join('. ') || ''
        };
        return newErrors;
    }, {});

    return textFieldErrors;
};

export const formatDate = date => {
    const month = (date.getMonth() + 1).toString();
    const monthFormatted = month.length < 2 ? `0${month}` : month;

    const day = date.getDate().toString();
    const dayFormatted = day.length < 2 ? `0${day}` : day;

    const year = date.getFullYear();

    return `${year}-${monthFormatted}-${dayFormatted}`;
};


export const ROLE_OPERADOR = 'ROLE_OPERADOR';
export const ROLE_GERENTE = 'ROLE_GERENTE';
export const ROLE_ADMINISTRADOR = 'ROLE_ADMINISTRADOR';

const GRANTED_RULES = {
    [FuncionariosLocation.toUrl()]: ROLE_OPERADOR,
    [SecretariaLocation.toUrl()]: ROLE_GERENTE,
    [RelatoriosLocation.toUrl()]: ROLE_GERENTE,
    [UsuariosLocation.toUrl()]: ROLE_ADMINISTRADOR,
};

const ROLE_HIERARCHY = {
    ROLE_GERENTE: [ROLE_OPERADOR],
    ROLE_ADMINISTRADOR: [ROLE_GERENTE]
};

function isChildRole(roleTarget, currentRole) {
    if (roleTarget === currentRole) {
        return true;
    }
    if (typeof (ROLE_HIERARCHY[currentRole]) === 'undefined') {
        return false;
    }

    for (let i = 0; i < ROLE_HIERARCHY[currentRole].length; i++) {
        if (isChildRole(roleTarget, ROLE_HIERARCHY[currentRole][i])) {
            return true;
        }
    }

    return false;
}

export const isGranted = (role, location) => isChildRole(GRANTED_RULES[location.toUrl()], role);