import React from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { savePDF } from '@progress/kendo-react-pdf';
import { withStyles } from '@material-ui/core/styles';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import SpeedDial from '@material-ui/lab/SpeedDial';
import SpeedDialAction from '@material-ui/lab/SpeedDialAction';
import SpeedDialIcon from '@material-ui/lab/SpeedDialIcon';
import EditIcon from '@material-ui/icons/Edit';
import SaveAltIcon from '@material-ui/icons/SaveAlt';
import TableChartIcon from '@material-ui/icons/TableChart';
import ErrorIcon from '@material-ui/icons/Error';
import Navegation from '../Navegation';
import {
    getError,
    getHasFetchingError
} from '../../reducer';

const styles = theme => ({
    speedDial: {
        position: 'absolute',
        bottom: theme.spacing.unit * 16,
        right: theme.spacing.unit * 3,
    }
});

class Relatorio extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            speedDialOpen: false,
            showFetchErrorSnack: false
        };

        this.handleErrorSnackClose = this.handleErrorSnackClose.bind(this);
        this.handleSpeedDialClick = this.handleSpeedDialClick.bind(this);
        this.handleSpeedDialClose = this.handleSpeedDialClose.bind(this);
        this.handleSpeedDialOpen = this.handleSpeedDialOpen.bind(this);
        this.handleExportPDF = this.handleExportPDF.bind(this);
        this.handleExportExcel = this.handleExportExcel.bind(this);

        this.downloadExcel = false;
        this.reportNode = React.createRef();
    }

    handleErrorSnackClose() {
        this.setState({
            showFetchErrorSnack: false
        })
    }

    handleSpeedDialOpen() {
        this.setState({
            speedDialOpen: true
        });
    }

    handleSpeedDialClose() {
        this.setState({
            speedDialOpen: false
        });
    }

    handleSpeedDialClick() {
        this.setState({
            speedDialOpen: !this.state.speedDialOpen
        });
    }

    handleExportPDF() {
        savePDF(ReactDOM.findDOMNode(this.reportNode.current), {
            paperSize: 'A4',
        });
        this.handleSpeedDialClose();
    }

    handleExportExcel() {
        this.setState({
            speedDialOpen: false
        });
        this.downloadExcel = true;
        this.forceUpdate(() => this.downloadExcel = false);
    }

    render() {
        const {
            classes,
            Content = () => null,
            hasFetchingError,
            error
        } = this.props;

        return (
            <React.Fragment>
                <Navegation/>
                <Content
                    ref={this.reportNode}
                    downloadExcel={this.downloadExcel}
                />
                <SpeedDial
                    ariaLabel='SpeedDial openIcon export'
                    className={classes.speedDial}
                    icon={<SpeedDialIcon openIcon={<EditIcon/>}/>}
                    open={this.state.speedDialOpen}
                    onBlur={this.handleSpeedDialClose}
                    onClick={this.handleSpeedDialClick}
                    onClose={this.handleSpeedDialClose}
                    onFocus={this.handleSpeedDialOpen}
                    onMouseEnter={this.handleSpeedDialOpen}
                    onMouseLeave={this.handleSpeedDialClose}
                >
                    <SpeedDialAction
                        icon={<SaveAltIcon/>}
                        tooltipTitle={'Exportar PDF'}
                        onClick={this.handleExportPDF}
                    />
                    <SpeedDialAction
                        icon={<TableChartIcon/>}
                        tooltipTitle={'Exportar Excel'}
                        onClick={this.handleExportExcel}
                    />
                </SpeedDial>
                <Snackbar
                    anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
                    autoHideDuration={5000}
                    open={this.state.showFetchErrorSnack && hasFetchingError}
                    onClose={this.handleErrorSnackClose}
                >
                    <SnackbarContent
                        className={classes.error}
                        message={
                            <span>
                                    <ErrorIcon/>
                                {error}
                                </span>
                        }
                    />
                </Snackbar>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    hasFetchingError: getHasFetchingError(state),
    error: getError(state),
});

const connectHOC = connect(
    mapStateToProps
);

export default connectHOC(withStyles(styles)(Relatorio));