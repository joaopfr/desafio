import React from 'react';
import { connect } from 'react-redux';
import ReactExport from 'react-data-export';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { fetchSalarioSecretariasReport } from '../../actions';
import {
    getUser,
    getSalarioSecretariasReport
} from '../../reducer';

class SalarioSecretariaReport extends React.Component {
    componentDidMount() {
        const {
            user,
            fetchSalarioSecretariasReport
        } = this.props;

        fetchSalarioSecretariasReport(user)();
    }

    render() {
        const {
            innerRef,
            downloadExcel,
            salarioSecretariasReport

        } = this.props;
        return (
            <React.Fragment>
                <Table ref={innerRef}>
                    <TableHead>
                        <TableRow>
                            <TableCell>
                                Secretaria
                            </TableCell>
                            <TableCell>
                                Salário Líquido Total dos Funcionários (Reais R$)
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {salarioSecretariasReport.map(salarioSecretaria => (
                            <TableRow key={salarioSecretaria.secretaria}>
                                <TableCell>
                                    {salarioSecretaria.secretaria}
                                </TableCell>
                                <TableCell>
                                    {salarioSecretaria.total || '0.00'}
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
                {
                    downloadExcel === true ?
                        <ReactExport.ExcelFile hideElement={true}>
                            <ReactExport.ExcelFile.ExcelSheet
                                data={salarioSecretariasReport}
                                name={`Salário Líquido Total dos Funcionários (Reais R$)`}
                            >
                                <ReactExport.ExcelFile.ExcelColumn
                                    label='Secretaria'
                                    value='secretaria'
                                />
                                <ReactExport.ExcelFile.ExcelColumn
                                    label={`Salário Líquido Total dos Funcionários (Reais R$)`}
                                    value={
                                        salarioSecretaria => salarioSecretaria.total === null ?
                                            "0.00" : salarioSecretaria.total
                                    }
                                />
                            </ReactExport.ExcelFile.ExcelSheet>
                        </ReactExport.ExcelFile> :
                        null
                }
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => ({
    salarioSecretariasReport: getSalarioSecretariasReport(state),
    user: getUser(state)
});

const mapDispatchToProps = dispatch => ({
    fetchSalarioSecretariasReport(user) {
        return () => dispatch(
            fetchSalarioSecretariasReport(user)
        );
    }
});

const connectHOC = connect(
    mapStateToProps,
    mapDispatchToProps
);

const SalarioSecretariaReportWithConnect = connectHOC(SalarioSecretariaReport);
const SalarioSecretariaReportRef = React.forwardRef(
    (props, ref) => <SalarioSecretariaReportWithConnect innerRef={ref} {...props}/>
);

export default Report => props => <Report Content={SalarioSecretariaReportRef} {...props}/>