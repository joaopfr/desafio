import React from 'react';
import { Formik, Form, Field } from 'formik';
import FormControl from '@material-ui/core/FormControl'
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from  '@material-ui/core/FormLabel';
import Grid from '@material-ui/core/Grid';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import TextField from '@material-ui/core/TextField';
import { mapErrors, formatDate } from '../utils';

const funcionarioPeridoAdmissaoExoneracaoInitialValues = {
    dateBegin: formatDate(new Date()),
    dateEnd: formatDate(new Date()),
    isAdmissao: 'true'
};

class FuncionariosPeriodoAdmissaoExoneracaoFormikComponent extends React.Component {
    componentDidMount() {
        if (this.props.isInitialValid === false) {
            this.props.setErrors(this.props.validationErrors);
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.hasValidationErrors === false && this.props.hasValidationErrors === true) {
            this.props.setErrors(this.props.validationErrors);
        }
    }

    render() {
        const { values, handleSubmit } = this.props;
        const textFieldErrors = mapErrors(this.props.errors);

        return (
            <Form>
                <Grid container spacing={40} justify='space-evenly'>
                    <Grid item xs={12} sm={6}>
                        <FormControl component='fieldset'>
                           <FormLabel component='legend'>Selecione o Período</FormLabel>
                           <Field name='dateBegin' render={({ field }) => (
                               <TextField
                                   label='Data Início'
                                   type='date'
                                   margin='normal'
                                   {...textFieldErrors.dateBegin}
                                   InputLabelProps={{ shrink: true }}
                                   {...field}
                               />
                           )}
                           />

                            <Field name='dateEnd' render={({ field }) => (
                                <TextField
                                    label='Data Final'
                                    type='date'
                                    margin='normal'
                                    {...textFieldErrors.dateEnd}
                                    InputLabelProps={{ shrink: true }}
                                    {...field}
                                />
                            )}/>
                        </FormControl>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <FormControl component='fieldset'>
                            <FormLabel component='legend'>Ativos ou Exonerados</FormLabel>
                            <Field name='isAdmissao' render={({ field }) => (
                                <RadioGroup {...field}>
                                    <FormControlLabel
                                        value='true'
                                        label='Período de Admissão'
                                        control={<Radio color='primary'/>}
                                    />
                                    <FormControlLabel
                                        value='false'
                                        label='Período de Exoneração'
                                        control={<Radio color='primary'/>}
                                    />
                                </RadioGroup>
                            )}/>
                        </FormControl>
                    </Grid>
                </Grid>
            </Form>
        );
    }
}

const FuncionariosPeriodoAdmissaoExoneracaoForm = React.forwardRef((props, ref) => {
    const {
        hasValidationErrors,
        validationFuncionarioPeriodo,
        funcionarioPeridoAdmissaoExoneracao,
        onSubmit,
        ...otherProps
    } = props;

    const initialValues = (hasValidationErrors === true && validationFuncionarioPeriodo) ||
        funcionarioPeridoAdmissaoExoneracao ||
        funcionarioPeridoAdmissaoExoneracaoInitialValues;

    return <Formik
        ref={ref}
        initialValues={initialValues}
        onSubmit={onSubmit}
        isInitialValid={hasValidationErrors === false}
        render={formikProps =>
            <FuncionariosPeriodoAdmissaoExoneracaoFormikComponent
                hasValidationErrors={hasValidationErrors}
                validationFuncionarioPeriodo={validationFuncionarioPeriodo}
                {...formikProps}
                {...otherProps}
            />
        }
    />
});

export default FuncionariosPeriodoAdmissaoExoneracaoForm;