import React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import ReactExport from 'react-data-export';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import Divider from '@material-ui/core/Divider';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import FuncionariosPeriodoAdmissaoExoneracaoForm from './FuncionariosPeriodoAdmissaoExoneracaoForm';
import * as actions from '../../actions';
import {
    getFuncionariosPeriodoAdmissaoExoneracao,
    getError,
    getHasFetchingError,
    getHasValidationErrors,
    getIsLoading,
    getUser,
    getValidationEntity,
    getValidationErrors
} from '../../reducer';

const styles = theme => ({
    heading: {
        fontSize: theme.typography.pxToRem(15)
    }
});

const FuncionariosPeriodoAdmissaoExoneracaoReport = React.forwardRef((props, ref) => {
    const {
        funcionariosPeriodoAdmissaoExoneracao,
        classes,
        downloadExcel,
        user,
        isLoading,
        onSubmit,
        ...otherProps
    } = props;

    const formNode = React.createRef();
    const onSubmitWithUser = onSubmit(user);

    return (
        <React.Fragment>
            <ExpansionPanel defaultExpanded>
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon/>}>
                    <Typography className={classes.heading}>
                        Configurar Relatório
                    </Typography>
                </ExpansionPanelSummary>
                <Divider/>
                <ExpansionPanelDetails>
                    <FuncionariosPeriodoAdmissaoExoneracaoForm
                        ref={formNode}
                        onSubmit={onSubmitWithUser}
                        {...otherProps}
                    />
                </ExpansionPanelDetails>
                <Divider/>
                <ExpansionPanelActions>
                    <Button size='small' onClick={() => formNode.current.submitForm()}>
                        Enviar
                    </Button>
                </ExpansionPanelActions>
            </ExpansionPanel>

            <Table ref={ref}>
                <TableHead>
                    <TableRow>
                        <TableCell>
                            Id
                        </TableCell>
                        <TableCell>
                            Nome
                        </TableCell>
                        <TableCell>
                            CPF
                        </TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {
                        funcionariosPeriodoAdmissaoExoneracao.map(f => (
                            <TableRow key={f.id}>
                                <TableCell>
                                    {f.id}
                                </TableCell>
                                <TableCell>
                                    {f.nome}
                                </TableCell>
                                <TableCell>
                                    {f.cpf}
                                </TableCell>
                            </TableRow>
                        ))
                    }
                </TableBody>
            </Table>

            {isLoading ? <CircularProgress size={100}/> : null}

            {
                downloadExcel === true ?
                    <ReactExport.ExcelFile hideElement={true}>
                        <ReactExport.ExcelFile.ExcelSheet
                            data={funcionariosPeriodoAdmissaoExoneracao}
                            name='Funcionários por período de Admissão / Exoneração'
                        >
                            <ReactExport.ExcelFile.ExcelColumn
                                label='Id'
                                value='id'
                            />
                            <ReactExport.ExcelFile.ExcelColumn
                                label='Nome'
                                value='nome'
                            />
                            <ReactExport.ExcelFile.ExcelColumn
                                label='CPF'
                                value='cpf'
                            />
                        </ReactExport.ExcelFile.ExcelSheet>
                    </ReactExport.ExcelFile>
                    : null
            }
        </React.Fragment>
    );
});

const mapStateToProps = state => ({
    funcionariosPeriodoAdmissaoExoneracao: getFuncionariosPeriodoAdmissaoExoneracao(state),
    user: getUser(state),
    isLoading: getIsLoading(state),
    hasValidationErrors: getHasValidationErrors(state),
    validationFuncionarioPeriodo: getValidationEntity(state),
    validationErrors: getValidationErrors(state)
});

const mapDispatchToProps = dispatch => ({
    onSubmit(user) {
        return values => dispatch(
            actions.fetchFuncionariosPeriodoAdmissaoExoneracao(user, values)
        );
    }
});

const connectHOC = connect(
    mapStateToProps,
    mapDispatchToProps
);

const stylesHOC = withStyles(styles);

const FuncionariosPeriodoAdmissaoExoneracaoReportStyles = stylesHOC(
    FuncionariosPeriodoAdmissaoExoneracaoReport
);

const FuncionariosPeriodoAdmissaoExoneracaoReportConnect = connectHOC(
    FuncionariosPeriodoAdmissaoExoneracaoReportStyles
);

const FuncionariosPeriodoAdmissaoExoneracaoReportRef = React.forwardRef(
    (props, ref) => <FuncionariosPeriodoAdmissaoExoneracaoReportConnect innerRef={ref} {...props}/>
);

export default Report => props => <Report
    Content={FuncionariosPeriodoAdmissaoExoneracaoReportRef}
    {...props}
/>;