import React from 'react';
import { Formik, Form, Field } from 'formik';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import Grid from '@material-ui/core/Grid';
import InputAdornment from '@material-ui/core/InputAdornment';
import MenuItem from '@material-ui/core/MenuItem';
import RadioGroup from '@material-ui/core/RadioGroup';
import Radio from '@material-ui/core/Radio';
import Switch from '@material-ui/core/Switch';
import TextField from '@material-ui/core/TextField';
import { mapErrors, formatDate } from '../utils';
import ImageUpload from '../ImageUpload';

const funcionarioInitialValue = {
    id: '',
    nome: '',
    cpf: '',
    secretaria: '',
    status: 'ATIVO',
    posse: 'ESTATUTARIO',
    salarioBase: '0',
    gratificacao: '0',
    desconto: '0',
    dataAdmissao: formatDate(new Date()),
    dataExoneracao: formatDate(new Date()),
    documentationMedias: []
};

class FuncionarioFormikComponent extends React.Component {
    constructor(props) {
        super(props);
        this.handleOnFileSelect = this.handleOnFileSelect.bind(this);
        this.handleOnFileRemove = this.handleOnFileRemove.bind(this);
    }

    componentDidMount() {
        if (this.props.isInitialValid === false) {
            this.props.setErrors(this.props.validationErrors);
        }
    }

    componentDidUpdate(prevProps, prevState) {
        // TODO maybe create an asyncErrors state in gDSFP, use it along with local errors .
        // TODO with asyncErrors keep the errors whist the form is not resubmited.
        // TODO local validadtion not implemented yet.
        if (prevProps.hasValidationErrors === false && this.props.hasValidationErrors === true) {
            this.props.setErrors(this.props.validationErrors);
        }
    }

    static getSalarioLiquido(salarioBase, gratificacao, desconto) {
        const salarioLiquido = parseFloat(salarioBase) + parseFloat(gratificacao)
            - parseFloat(desconto);

        return isNaN(salarioLiquido) ? '' :Number(Math.round(salarioLiquido + 'e2') + 'e-2');
    }

    handleOnFileSelect(selectedFiles, field, formikBag) {
        const values = this.props.values;
        const files = [];
        for (const f of selectedFiles) {
            f.status = 'ADDED';
            files.push(f);
        }

        formikBag.setFieldValue(field.name, [
            ...values.documentationMedias, ...files
        ])
    }

    handleOnFileRemove(files, file, field, formikBag) {
        file.status = 'REMOVED';

        formikBag.setFieldValue(
            field.name,
            files.map(
                f => f.name === file.name ? file : f
            )
        );
    }

    render() {
        const {
            errors,
            values,
            handleSubmit,
            isReadOnly,
            isAdd,
            secretarias
        } = this.props;

        const readOnly = isReadOnly;
        const textFieldErrors = mapErrors(errors);

        values.gratificacao = values.posse === 'COMISSIONADO' ? '0' : values.gratificacao;
        const salarioLiquido = FuncionarioFormikComponent.getSalarioLiquido(values.salarioBase, values.gratificacao, values.desconto);

        return (
            <Form>
                <Grid container spacing={16} alignItems='flex-end'>
                    {
                        isAdd ? null :
                            <Grid item xs={12}>
                                <Field name='id' render={({ field }) => (
                                    <TextField fullWidth label='Id' margin='normal' InputProps={{ readOnly: true }} {...field}/>
                                )}/>
                            </Grid>
                    }
                    <Grid item xs={12} sm={6}>
                        <Field name='nome' render={({ field }) => (
                            <TextField
                                label='Nome'
                                margin='normal'
                                {...textFieldErrors.nome}
                                InputProps={{ readOnly }}
                                {...field}
                            />
                        )}/>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <Field name='cpf' render={({ field }) => (
                            <TextField
                                label='CPF'
                                margin='normal'
                                {...textFieldErrors.cpf}
                                InputProps={{ readOnly }}
                                {...field}
                            />
                        )}/>
                    </Grid>
                    <Grid item xs={12} sm={12}>
                        <Field name='secretaria' render={({ field }) => (
                            <TextField
                                select
                                fullWidth
                                label='Secretaria'
                                margin='normal'
                                {...textFieldErrors.secretaria}
                                InputProps={{ readOnly }}
                                InputLabelProps={{ shrink: true }}
                                {...field}
                            >
                                {
                                    secretarias.map(sec => (
                                        <MenuItem key={sec.id} value={sec.id}>
                                            {sec.nome}
                                        </MenuItem>
                                    ))
                                }
                            </TextField>
                        )}/>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <Field name='posse' render={({ field }) => (
                            <FormControl component='fieldset'>
                                <FormLabel component='legend'>Tipo de Posse</FormLabel>
                                <RadioGroup {...field}>
                                    <FormControlLabel
                                        value='ESTATUTARIO'
                                        label='Estatutário'
                                        control={<Radio color='primary' disabled={readOnly}/>}
                                    />
                                    <FormControlLabel
                                        value='COMISSIONADO'
                                        label='Comissionado'
                                        control={<Radio color='primary' disabled={readOnly}/>}
                                    />
                                </RadioGroup>
                            </FormControl>
                        )}/>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <FormControl component='fieldset'>
                            <FormLabel component='legend'>Condição do Funcionário</FormLabel>
                            <Field name='status' render={({ field, form: formBag }) => (
                                <FormControlLabel
                                    control={
                                        <Switch
                                            {...field}
                                            disabled={readOnly}
                                            color='primary'
                                            checked={field.value === 'ATIVO'}
                                            onChange={e => e.target.checked === true ?
                                                formBag.setFieldValue(field.name, 'ATIVO') :
                                                formBag.setFieldValue(field.name, 'EXONERADO')
                                            }
                                        />
                                    }
                                    label={values.status === 'ATIVO' ? 'Ativo' : 'Exonerado'}
                                />
                            )}/>
                            <Field name='dataAdmissao' render={({ field }) => (
                                <TextField
                                    label='Data Admissão'
                                    type='date'
                                    margin='normal'
                                    {...textFieldErrors.dataAdmissao}
                                    InputProps={{ readOnly }}
                                    InputLabelProps={{ shrink: true }}
                                    {...field}
                                />
                            )}/>
                            {
                                values.status === 'EXONERADO' ?
                                    <Field name='dataExoneracao' render={({ field }) => (
                                        <TextField
                                            label='Data Exeoneração'
                                            type='date'
                                            margin='normal'
                                            {...textFieldErrors.dataExoneracao}
                                            InputProps={{ readOnly }}
                                            InputLabelProps={{ shrink: true }}
                                            {...field}
                                        />
                                    )}/> : null
                            }
                        </FormControl>
                    </Grid>
                    <Grid item xs={12}>
                        <FormLabel component='legend'>Remuneração</FormLabel>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <Field name='salarioBase' render={({ field }) => (
                            <TextField
                                label='Salário Base'
                                margin='normal'
                                InputProps={{
                                    readOnly,
                                    startAdornment: <InputAdornment position='start'>R$</InputAdornment>
                                }}
                                {...textFieldErrors.salarioBase}
                                {...field}
                            />
                        )}/>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <Field name='gratificacao' render={({ field }) =>(
                            <TextField
                                label='Gratificação'
                                margin='normal'
                                disabled={values.posse === 'COMISSIONADO'}
                                InputProps={{
                                    readOnly: readOnly || values.posse === 'COMISSIONADO',
                                    startAdornment: <InputAdornment position='start'>R$</InputAdornment>
                                }}
                                {...textFieldErrors.gratificacao}
                                {...field}
                            />
                        )}/>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <Field name='desconto' render={({ field }) => (
                            <TextField
                                label='Desconto'
                                margin='normal'
                                InputProps={{
                                    readOnly,
                                    startAdornment: <InputAdornment position='start'>R$</InputAdornment>
                                }}
                                {...textFieldErrors.desconto}
                                {...field}
                            />
                        )}/>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            label='Salário Líquido'
                            margin='normal'
                            InputProps={{
                                readOnly: true,
                                startAdornment: <InputAdornment position='start'>R$</InputAdornment>
                            }}
                            value={salarioLiquido}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <FormLabel component='legend'>Documentação</FormLabel>
                    </Grid>
                    <Grid item xs={12}>
                        <Field name='documentationMedias' render={({ field, form: formikBag }) => (
                            <ImageUpload
                                readOnly={readOnly}
                                files={field.value}
                                onFileSelect={
                                    files => this.handleOnFileSelect(files, field, formikBag)
                                }
                                onFileRemove={
                                    file => this.handleOnFileRemove(field.value, file, field, formikBag)
                                }
                                {...textFieldErrors.documentationMedias}
                            />
                        )}/>
                    </Grid>
                </Grid>
            </Form>
        )
    }
}

const FuncionarioForm = React.forwardRef((props, ref) => {
    const {
        funcionario,
        onSubmit,
        hasValidationErrors,
        validationFuncionario,
        ...otherProps
    } = props;

    const formFuncionario = funcionario && {
        ...funcionario,
        secretaria: funcionario.secretaria.id,
        status: funcionario.status.status,
        posse: funcionario.posse.tipo
    };

    const initialValues = (
        hasValidationErrors === true && validationFuncionario
    ) || formFuncionario || funcionarioInitialValue;

    return <Formik
        ref={ref}
        initialValues={initialValues}
        onSubmit={onSubmit}
        isInitialValid={hasValidationErrors === false}
        render={formikProps =>
            <FuncionarioFormikComponent
                hasValidationErrors={hasValidationErrors}
                validationFuncionario={validationFuncionario}
                {...formikProps}
                {...otherProps}
            />
        }
    />
});

export default FuncionarioForm;