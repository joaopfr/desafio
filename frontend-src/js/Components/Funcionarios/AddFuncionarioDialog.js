import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import CircularProgress from '@material-ui/core/CircularProgress';
import FuncionarioForm from './FuncionarioForm';

export const AddFuncionarioDialog = props => {
    const {
        open,
        onClose,
        onAddFuncionario,
        isLoading,
        ...others
    } = props;

    let formNode = React.createRef();
    const onAddFuncionarioThenClose = newFuncionarioValues => {
        onAddFuncionario(newFuncionarioValues).then(() => onClose());
    };

    return (
        <Dialog open={open} onClose={onClose}>
            <DialogTitle>Adicionar Funcionário</DialogTitle>
            <DialogContent>
                <FuncionarioForm
                    ref={formNode}
                    isReadOnly={false}
                    isAdd={true}
                    onSubmit={onAddFuncionarioThenClose}
                    {...others}
                />
            </DialogContent>

            {isLoading ? <CircularProgress size={100}/> : null}

            <DialogActions>
                <Button variant='outlined' color='primary' onClick={onClose}>
                    Cancelar
                </Button>
                <Button variant='contained' color='primary' onClick={() => {
                    formNode.current.submitForm();
                }}>
                    Criar
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export const withAddFuncionarioDialog = Funcionarios => props => <Funcionarios
    CrudDialog={AddFuncionarioDialog}
    {...props}
/>;
