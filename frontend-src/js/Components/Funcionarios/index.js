import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';
import Fab from "@material-ui/core/Fab";
import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import ErrorIcon from '@material-ui/icons/Error';
import EntityTable from '../EntityTable';
import NavegationBar from  '../Navegation/index';
import * as actions from '../../actions/index';
import * as funcionariosLocation from '../../routes/funcionariosLocations';
import {
    getHasFetchingError,
    getIsLoading,
    getFuncionarios,
    getSecretariasSelector,
    getUser,
    getError,
    getHasValidationErrors,
    getValidationEntity,
    getValidationErrors
} from '../../reducer';

const styles = theme => ({
    error: {
        backgroundColor: theme.palette.error.dark
    },
    fab: {
        position: 'absolute',
        bottom: theme.spacing.unit * 16,
        right: theme.spacing.unit * 3,
    }
});

const header= ['Id', 'Nome', 'CPF'];
const attrs = ['id', 'nome', 'cpf'];

const locations = {
    ViewLocation: funcionariosLocation.ViewFuncionarioLocation,
    EditLocation: funcionariosLocation.EditFuncionarioLocation,
    RemoveLocation: funcionariosLocation.RemoveFuncionarioLocation
};

class Funcionarios extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showFetchErrorSnack: true
        };
        this.handleErrorSnackClose = this.handleErrorSnackClose.bind(this);
    }
    componentDidMount() {
        const { match } = this.props;

        const isFuncionariosLocation = match.path === funcionariosLocation.FuncionariosLocation.toUrl();
        const isRootLocation = match.path === '/';

        if (isFuncionariosLocation || isRootLocation) {
            this.props.fetchFuncionarios(this.props.user)();
            this.props.fetchSecretariasSelector(this.props.user)();
        }
    }

    handleErrorSnackClose() {
        this.setState({
            showFetchErrorSnack: false
        })
    }

    render() {
        const {
            classes,
            match,
            history,
            funcionarios,
            secretariasSelector,
            user,
            isLoading,
            hasFetchingError,
            error,
            hasValidationErrors,
            validationFuncionario,
            validationErrors,
            CrudDialog = () => null,
            onAddFuncionario,
            onEditFuncionario,
            onRemoveFuncionario,
            clearValidationErrors
        } = this.props;

        const onDialogActions = {
            onAddFuncionario: onAddFuncionario(user),
            onEditFuncionario: onEditFuncionario(user),
            onRemoveFuncionario: onRemoveFuncionario(user)
        };

        const onCloseDialog = () => clearValidationErrors().then(
            () => history.push(funcionariosLocation.FuncionariosLocation.path)
        );
        const funcionario = typeof (match.params.id) === 'undefined' ? null :
            funcionarios.filter(f => f.id === parseInt(match.params.id))[0];

        return (
            <React.Fragment>
                <NavegationBar/>
                    <EntityTable entityLocations={locations}
                                 entityHeader={header}
                                 entityAttrs={attrs}
                                 entityData={funcionarios}
                    />

                    {isLoading ? <CircularProgress size={100}/> : null}


                    <Link to={funcionariosLocation.AddFuncionarioLocation.toUrl()}>
                        <Fab color='primary' className={classes.fab}>
                            <AddIcon/>
                        </Fab>
                    </Link>

                    <CrudDialog
                        funcionario={funcionario}
                        secretarias={secretariasSelector}
                        open={true}
                        onClose={onCloseDialog}
                        isLoading={isLoading}
                        hasValidationErrors={hasValidationErrors}
                        validationFuncionario={validationFuncionario}
                        validationErrors={validationErrors}
                        {...onDialogActions}
                    />

                    <Snackbar
                        anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
                        autoHideDuration={5000}
                        open={this.state.showFetchErrorSnack && hasFetchingError}
                        onClose={this.handleErrorSnackClose}
                    >
                        <SnackbarContent
                            className={classes.error}
                            message={
                                <span>
                                    <ErrorIcon/>
                                    {error}
                                </span>
                            }
                        />
                    </Snackbar>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    funcionarios: getFuncionarios(state),
    secretariasSelector: getSecretariasSelector(state),
    user: getUser(state),
    hasFetchingError: getHasFetchingError(state),
    error: getError(state),
    isLoading: getIsLoading(state),
    hasValidationErrors: getHasValidationErrors(state),
    validationFuncionario: getValidationEntity(state),
    validationErrors: getValidationErrors(state)
});

const mapDispatchToProps = dispatch => ({
    fetchFuncionarios(user) {
        return () => dispatch(
            actions.fetchFuncionarios(user)
        );
    },

    fetchSecretariasSelector(user) {
        return () => dispatch(
            actions.fetchSecretariasSelector(user)
        );
    },

    onAddFuncionario(user) {
        return newFuncionario => dispatch(
            actions.addFuncionario({
                ...newFuncionario,
                dataExoneracao: newFuncionario.status === 'ATIVO' ?
                    null : newFuncionario.dataExoneracao
            }, user)
        );
    },

    onEditFuncionario(user) {
        return funcionario => dispatch(
            actions.editFuncionario({
                ...funcionario,
                dataExoneracao: funcionario.status === 'ATIVO' ?
                    null : funcionario.dataExoneracao
            }, user)
        );
    },

    onRemoveFuncionario(user) {
        return id => dispatch(
            actions.removeFuncionario(id, user)
        );
    },

    clearValidationErrors() {
        return dispatch(actions.clearValidationErrors());
    }
});

const componentStyles = withStyles(styles);
const componentReduxProps = connect(
    mapStateToProps,
    mapDispatchToProps
);

export default componentReduxProps(
    componentStyles(Funcionarios)
);