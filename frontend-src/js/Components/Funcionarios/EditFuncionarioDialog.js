import React from 'react';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import FuncionarioForm from './FuncionarioForm';

export const EditFuncionarioDialog = props => {
    const {
        open,
        onClose,
        onEditFuncionario,
        isLoading,
        ...otherProps
    } = props;

    let formNode = React.createRef();
    const onEditFuncionarioThenClose = funcionario => {
        onEditFuncionario(funcionario).then(() => onClose());
    };

    return (
        <Dialog open={open} onClose={onClose}>
            <DialogTitle>Editar Funcionário</DialogTitle>
            <DialogContent>
                <FuncionarioForm
                    ref={formNode}
                    isReadOnly={false}
                    isAdd={false}
                    onSubmit={onEditFuncionarioThenClose}
                    {...otherProps}
                />
            </DialogContent>

            {isLoading ? <CircularProgress size={100}/> : null}

            <DialogActions>
                <Button variant='outlined' color='primary' onClick={onClose}>
                    Cancelar
                </Button>
                <Button variant='contained' color='primary' onClick={() => {
                    formNode.current.submitForm();
                }}>
                    Editar
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export const withEditFuncionarioDialog = Funcionarios => props => <Funcionarios
    CrudDialog={EditFuncionarioDialog}
    {...props}
/>;