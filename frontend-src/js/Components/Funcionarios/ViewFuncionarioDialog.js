import React from 'react';
import FuncionarioForm from './FuncionarioForm';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';

export const ViewFuncionarioDialog = ({ funcionario, secretarias, open, onClose }) => {
    return (
        <Dialog open={open} onClose={onClose}>
            <DialogTitle>Visualizar Funcionário</DialogTitle>
            <DialogContent>
                <FuncionarioForm funcionario={funcionario} secretarias={secretarias} isReadOnly={true} isAdd={false}/>
            </DialogContent>
            <DialogActions>
                <Button variant='contained' color='primary' onClick={onClose}>
                    Fechar
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export const withViewFuncioarioDialog = Funcionarios => props => <Funcionarios
    CrudDialog={ViewFuncionarioDialog}
    {...props}
/>;
