import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogActions';
import Slide from '@material-ui/core/Slide';

const Transition = props => <Slide direction='up' {...props}/>;

export const RemoveFuncionarioDialog = ({ funcionario, open, onClose, onRemoveFuncionario }) => (
    <Dialog open={open} TransitionComponent={Transition} onClose={onClose}>
        <DialogTitle>Remover Funcionário</DialogTitle>
        <DialogContent>
            <DialogContentText>
                {
                    `Você confirma a remoção do Funcionário ${funcionario.nome} com id ${funcionario.id}?`
                }
            </DialogContentText>
        </DialogContent>
        <DialogActions>
            <Button variant='outlined' color='primary' onClick={onClose}>
                Cancelar
            </Button>
            <Button variant='contained' color='primary' onClick={() => {
                onRemoveFuncionario(funcionario.id).then(
                    () => onClose()
                ).catch(
                    () => onClose()
                );
            }}>
                Remover
            </Button>
        </DialogActions>
    </Dialog>
);

export const withRemoveFuncionarioDialog = Funcionarios => props => <Funcionarios
    CrudDialog={RemoveFuncionarioDialog}
    {...props}
/>;