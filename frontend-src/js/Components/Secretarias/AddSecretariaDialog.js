import React from 'react';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogContent';
import SecretariaForm from './SecretariaForm';

export const AddSecretariaDialog = props => {
    const {
        open,
        onClose,
        onAddSecretaria,
        isLoading,
        hasValidationErrors,
        validationSecretaria,
        validationErrors
    } = props;

    const formNode = React.createRef();
    const onSubmit = newSecretaria => {
        onAddSecretaria(newSecretaria).then(() => onClose());
    };

    return (
        <Dialog open={open} onClose={onClose}>
            <DialogTitle>Adicionar Secretaria</DialogTitle>
            <DialogContent>
                <SecretariaForm
                    ref={formNode}
                    isAdd={true}
                    isReadOnly={false}
                    onSubmit={onSubmit}
                    hasValidationErrors={hasValidationErrors}
                    validationSecretaria={validationSecretaria}
                    validationErrors={validationErrors}
                />
            </DialogContent>

            {isLoading ? <CircularProgress size={100}/> : null}

            <DialogActions>
                <Button variant='outlined' color='primary' onClick={onClose}>
                    Cancelar
                </Button>
                <Button variant='contained' color='primary' onClick={() => {
                    formNode.current.submitForm();
                }}>
                    Adicionar
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export const withAddSecretariaDialog = Secretarias => props => <Secretarias
    CrudDialog={AddSecretariaDialog} {...props}
/>;