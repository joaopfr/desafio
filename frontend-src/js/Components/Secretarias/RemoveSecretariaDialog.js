import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogActions';
import Slide from '@material-ui/core/Slide';

const Transition = (props) => <Slide direction='up' {...props}/>;

export const RemoveSecretariaDialog = ({ secretaria, open, onClose, onRemoveSecretaria }) => {
    return (
        <Dialog open={open} onClose={onClose} TransitionComponent={Transition}>
            <DialogTitle> Remover Secretaria</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    {`Você confirma a remoção da Secretaria ${secretaria.nome} com Id ${secretaria.id}?`}
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button variant='outlined' color='primary' onClick={onClose}>
                    Cancelar
                </Button>
                <Button variant='contained' color='primary' onClick={() => {
                    onRemoveSecretaria(secretaria.id);
                    onClose();
                }}>
                    Remover
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export const withRemoveSecretariaDialog = Secretarias => props => <Secretarias
    CrudDialog={RemoveSecretariaDialog} {...props}
/>;