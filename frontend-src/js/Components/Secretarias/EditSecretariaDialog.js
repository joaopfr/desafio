import React from 'react';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import SecretariaForm from './SecretariaForm';

export const EditSecretariaDialog = props => {
    const {
        secretaria,
        open,
        onClose,
        onEditSecretaria,
        isLoading,
        hasValidationErrors,
        validationSecretaria,
        validationErrors
    } = props;

    const formNode = React.createRef();
    const onSubmit = secretaria => {
        onEditSecretaria(secretaria).then(() => onClose());
    };

    return (
        <Dialog open={open} onClose={onClose}>
            <DialogTitle>Editar Secretaria</DialogTitle>
            <DialogContent>
                <SecretariaForm
                    secretaria={secretaria}
                    ref={formNode}
                    isAdd={false}
                    isReadOnly={false}
                    onSubmit={onSubmit}
                    hasValidationErrors={hasValidationErrors}
                    validationSecretaria={validationSecretaria}
                    validationErrors={validationErrors}
                />
            </DialogContent>

            {isLoading ? <CircularProgress size={100}/> : null}

            <DialogActions>
                <Button variant='outlined' color='primary' onClick={onClose}>
                    Cancelar
                </Button>
                <Button variant='contained' color='primary' onClick={() => {
                    formNode.current.submitForm();
                }}>
                    Editar
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export const withEditSecretariaDialog = Secretarias => props => <Secretarias
    CrudDialog={EditSecretariaDialog} {...props}
/>;