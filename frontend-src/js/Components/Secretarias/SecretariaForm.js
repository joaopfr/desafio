import React from 'react';
import { Formik, Form, Field } from 'formik';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';

const secretariaInitialValues = {
    id: '',
    nome: '',
};

class SecretariaFormikComponent extends React.Component {
    componentDidMount() {
        if (this.props.isInitialValid === false) {
            this.props.setErrors(this.props.validationErrors);
        }
    }

    componentDidUpdate(prevProps, prevState) {
        // TODO maybe create an asyncErrors state in gDSFP, use it along with local errors .
        // TODO with asyncErrors keep the errors whist the form is not resubmited.
        // TODO local validadtion not implemented yet.
        if (prevProps.hasValidationErrors === false && this.props.hasValidationErrors === true) {
            this.props.setErrors(this.props.validationErrors);
        }
    }

    mapErrors() {
        const { errors } = this.props;
        const textFieldErrors = Object.keys(errors).reduce((newErrors, errorKey) => {
            newErrors[errorKey] = {
                error: typeof(errors[errorKey]) !== 'undefined' && errors[errorKey].length > 0,
                helperText: errors[errorKey] && errors[errorKey].join('. ') || ''
            };
            return newErrors;
        }, {});

        return textFieldErrors;
    }

    render() {
        const { values, handleSubmit, isReadOnly, isAdd } = this.props;
        const readOnly = isReadOnly;

        const textFieldErrors = this.mapErrors();

        return (
            <Form>
                <Grid container spacing={16}>
                    {
                        isAdd ? null :
                            <Grid item xs={12} sm={6}>
                                <Field name='id' render={({ field }) => (
                                    <TextField label='Id' margin='normal' InputProps={{readOnly: true}} {...field}/>
                                )}/>
                            </Grid>
                    }
                    <Grid item xs={12} sm={6}>
                        <Field name='nome' render={({ field }) => (
                            <TextField label='Nome' margin='normal' {...textFieldErrors.nome} InputProps={{ readOnly }} {...field}/>
                        )}/>
                    </Grid>
                </Grid>
            </Form>
        );
    }
}

const SecretariaForm = React.forwardRef(({ secretaria, onSubmit, hasValidationErrors, validationSecretaria, ...otherProps }, ref) => {
    const initialValues = (hasValidationErrors === true && validationSecretaria) || secretaria || secretariaInitialValues;

    return (
        <Formik
            ref={ref}
            initialValues={initialValues}
            onSubmit={onSubmit}
            isInitialValid={hasValidationErrors === false}
            render={formikProps =>
                <SecretariaFormikComponent
                    hasValidationErrors={hasValidationErrors}
                    validationSecretaria={validationSecretaria}
                    {...formikProps}
                    {...otherProps}
                />
            }
        />
    );
});

export default SecretariaForm;