import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import SecretariaForm from './SecretariaForm';

export const ViewSecretariaDialog = ({ secretaria, open, onClose }) => {
    return (
        <Dialog open={open} onClose={onClose}>
            <DialogTitle>Visualizar Secretaria</DialogTitle>
            <DialogContent>
                <SecretariaForm secretaria={secretaria} isAdd={false} isReadOnly={true}/>
            </DialogContent>
            <DialogActions>
                <Button variant='contained' color='primary' onClick={onClose}>
                    Fechar
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export const withViewSecretariaDialog = Secretarias => props => <Secretarias
    CrudDialog={ViewSecretariaDialog} {...props}
/>;