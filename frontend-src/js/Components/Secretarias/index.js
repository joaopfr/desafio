import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import ErrorIcon from '@material-ui/icons/Error';
import EntityTable from '../EntityTable';
import NavegationBar from '../Navegation/index';
import * as secretariasLocations from '../../routes/secretariasLocations';
import * as actions from '../../actions';
import {
    getSecretarias,
    getUser,
    getHasFetchingError,
    getIsLoading,
    getError,
    getHasValidationErrors,
    getValidationEntity,
    getValidationErrors
} from '../../reducer';

const styles = theme => ({
    error: {
        backgroundColor: theme.palette.error.dark
    },
    fab: {
        position: 'absolute',
        bottom: theme.spacing.unit * 16,
        right: theme.spacing.unit * 3,
    }
});

const header= ['Id', 'Nome'];
const attrs = ['id', 'nome'];

const locations = {
    ViewLocation: secretariasLocations.ViewSecretariaLocation,
    EditLocation: secretariasLocations.EditSecretariaLocation,
    RemoveLocation: secretariasLocations.RemoveSecretariaLocation
};

class Secretarias extends React.Component {
    constructor(props) {
        super(props);
        this.handleSnackClose = this.handleSnackClose.bind(this);

        this.state = {
            showFetchErrorSnack: true
        };
    }

    componentDidMount() {
        const {
            match,
            user,
            fetchSecretarias
        } = this.props;

        if (match.path === secretariasLocations.SecretariaLocation.toUrl()) {
            fetchSecretarias(user)();
        }
    }

    handleSnackClose() {
        this.setState({
            showFetchErrorSnack : false
        });
    }

    render() {
        const {
            classes,
            match,
            secretarias,
            user,
            isLoading,
            hasFetchingError,
            error,
            hasValidationErrors,
            validationFuncionario,
            validationErrors,
            history,
            CrudDialog = () => null,
            onAddSecretaria,
            onEditSecretaria,
            onRemoveSecretaria,
            clearValidationErrors
        } = this.props;

        const onDialogActions = {
            onAddSecretaria: onAddSecretaria(user),
            onEditSecretaria: onEditSecretaria(user),
            onRemoveSecretaria: onRemoveSecretaria(user)
        };

        const onClose = () => clearValidationErrors().then(() =>
            history.push(secretariasLocations.SecretariaLocation.toUrl())
        );

        const secretaria = typeof (match.params.id) === 'undefined' ? null : secretarias.filter(
            s => s.id === parseInt(match.params.id)
        )[0];

        return (
            <React.Fragment>
                <NavegationBar/>
                <EntityTable entityLocations={locations}
                             entityHeader={header}
                             entityAttrs={attrs}
                             entityData={secretarias}
                />

                {isLoading ? <CircularProgress size={100}/> : null}


                <Link to={secretariasLocations.AddSecretariaLocation.toUrl()}>
                    <Fab color='primary' className={classes.fab}>
                        <AddIcon/>
                    </Fab>
                </Link>

                <CrudDialog
                    secretaria={secretaria}
                    open={true}
                    onClose={onClose}
                    isLoading={isLoading}
                    hasValidationErrors={hasValidationErrors}
                    validationFuncionario={validationFuncionario}
                    validationErrors={validationErrors}
                    {...onDialogActions}/>

                <Snackbar
                    anchorOrigin={{vertical: 'top', horizontal: 'center'}}
                    autoHideDuration={5000}
                    open={this.state.showFetchErrorSnack && hasFetchingError}
                    onClose={this.handleSnackClose}
                >
                    <SnackbarContent
                        className={classes.error}
                        message={
                            <span>
                            <ErrorIcon/>
                                {error}
                        </span>
                        }
                    />
                </Snackbar>
            </React.Fragment>
        );
    }
};

const mapStateToProps = state => ({
    secretarias: getSecretarias(state),
    user: getUser(state),
    isLoading: getIsLoading(state),
    hasFetchingError: getHasFetchingError(state),
    error: getError(state),
    hasValidationErrors: getHasValidationErrors(state),
    validationFuncionario: getValidationEntity(state),
    validationErrors: getValidationErrors(state)
});

const mapDispatchToProps = dispatch => ({
    fetchSecretarias(user) {
        return () => dispatch(actions.fetchSecretarias(user))
    },

    onAddSecretaria(user) {
        return newSecretariaValues => dispatch(
            actions.addSecretaria(newSecretariaValues, user)
        );
    },

    onEditSecretaria(user) {
        return secretaria => dispatch(
            actions.editSecretaria(secretaria, user)
        );
    },

    onRemoveSecretaria(user) {
        return id => dispatch(
            actions.removeSecretaria(id, user)
        );
    },

    clearValidationErrors() {
        return dispatch(actions.clearValidationErrors());
    }
});

const componentStyles = withStyles(styles);
const componentReduxProps = connect(
    mapStateToProps,
    mapDispatchToProps
);

export default componentReduxProps(componentStyles(Secretarias));