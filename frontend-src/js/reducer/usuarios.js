import * as actionTypes from '../actions/types';

const usuarios = (state = [], action) => {
    switch (action.type) {
        case actionTypes.FETCH_USUARIOS:
            return action.usuarios;
        case actionTypes.ADD_USUARIO:
            return [
                ...state,
                action.usuario
            ];
        case actionTypes.EDIT_USUARIO:
            return state.map(
                u => u.id === action.usuario.id ?
                    action.usuario : u
            );
        case actionTypes.REMOVE_USUARIO:
            return state.filter(u => u.id !== action.id);
        default:
            return state;
    }
};

export default usuarios;

export const getUsuarios = state => state.usuarios;
