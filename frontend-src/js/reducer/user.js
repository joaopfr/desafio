import * as actionTypes from '../actions/types';

const user = (state = {}, action) => {
    switch (action.type) {
        case actionTypes.USER_SIGNIN:
            return action.user;
        case actionTypes.USER_LOGOUT:
            return {};
        default:
            return state;
    }
};

export default user;

export const getIsAuthenticated = state => typeof(state.user.token) !== 'undefined';
export const getUser = state => state.user;
