import { combineReducers } from 'redux';
import * as actionTypes from '../actions/types';

const error = (state = '', action) => {
    switch (action.type) {
        case actionTypes.REMOTE_REQUEST:
        case actionTypes.REMOTE_REQUEST_SUCCESS:
        case actionTypes.CLEAR_REMOTE_REQUEST_ERROR:
            return '';
        case actionTypes.REMOTE_REQUEST_ERROR:
        case actionTypes.REMOTE_AUTH_ERROR:
            return action.message;
        default:
            return state;
    }
};

const loading = (state = false, action) => {
    switch (action.type) {
        case actionTypes.REMOTE_REQUEST:
            return true;
        case actionTypes.REMOTE_REQUEST_SUCCESS:
        case actionTypes.REMOTE_REQUEST_ERROR:
        case actionTypes.REMOTE_VALIDATION_ERROR:
        case actionTypes.REMOTE_AUTH_ERROR:
            return false;
        default:
            return state;
    }
};

const fetching = combineReducers({
    loading,
    error
});

export const getIsLoading = state => state.fetching.loading;
export const getHasFetchingError = state => state.fetching.error !== '';
export const getError = state => state.fetching.error;

export default fetching;