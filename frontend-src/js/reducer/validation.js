import * as actionTypes from '../actions/types';

const validation = (state = {}, action) => {
    switch (action.type) {
        case actionTypes.REMOTE_VALIDATION_ERROR:
            return {
                entity: action.entity,
                errors: action.errors
            };
        case actionTypes.REMOTE_REQUEST_SUCCESS:
        case actionTypes.REMOTE_REQUEST:
        case actionTypes.REMOTE_REQUEST_ERROR:
        case actionTypes.CLEAR_VALIDATION_ERROR:
            return {};
        default:
            return state;
    }
};

export default validation;

export const getValidationEntity = state => state.validation.entity;
export const getValidationErrors = state => state.validation.errors;
export const getHasValidationErrors = state => typeof(state.validation.entity) !== 'undefined' && typeof(state.validation.errors) !== 'undefined';
