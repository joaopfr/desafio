import * as actionTypes from '../actions/types';

const mediaDocumentation = (state = null, action) => {
    switch (action.type) {
        case actionTypes.START_DOWNLOAD_MEDIA_DOCUMENTATION:
            return action.mediaDocumentation;
        case actionTypes.STOP_DOWNLOAD_MEDIA_DOCUMENTATION:
            return null;
        default:
            return state;
    }
};

export default mediaDocumentation;

export const getHasStartedMediaDocumentationDownload = state => state.mediaDocumentation !== null;
export const getMediaDocumentation = state => state.mediaDocumentation;