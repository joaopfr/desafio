import { combineReducers } from 'redux';
import * as actionTypes from '../actions/types';

const funcionarios = (state = [], action) => {
    switch (action.type) {
        case actionTypes.FETCH_FUNCIONARIOS:
            return action.funcionarios;
        case actionTypes.ADD_FUNCIONARIO:
            return [
                ...state,
                {
                    ...action.funcionario
                }
            ];
        case actionTypes.EDIT_FUNCIONARIO:
            return state.map(
                f => f.id === action.funcionario.id ? {...action.funcionario} : f
            );
        case actionTypes.REMOVE_FUNCIONARIO:
            return state.filter(f => f.id !== action.id);
        default:
            return state;
    }
};

const funcionariosPeriodoAdmissaoExoneracao = (state = [], action) => {
    switch (action.type) {
        case actionTypes.FETCH_FUNCIONARIOS_PERIODO_ADMISSAO_EXONERACAO:
            return action.funcionariosPeriodoAdmissaoExoneracao;
        default:
            return state;
    }
};

export const getFuncionarios = state => state.funcionarios.funcionarios;
export const getFuncionariosPeriodoAdmissaoExoneracao = state => state.funcionarios.funcionariosPeriodoAdmissaoExoneracao;

export default combineReducers({
    funcionarios,
    funcionariosPeriodoAdmissaoExoneracao
});