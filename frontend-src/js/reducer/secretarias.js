import { combineReducers } from 'redux';
import * as actionTypes from '../actions/types';

const secretarias = (state = [], action) => {
    switch(action.type) {
        case actionTypes.FETCH_SECRETARIAS:
            return action.secretarias;
        case actionTypes.ADD_SECRETARIA:
            return [
                ...state,
                {
                    ...action.secretaria
                }
            ];
        case actionTypes.EDIT_SECRETARIA:
            return state.map(
                s => s.id === action.secretaria.id ?
                    {...action.secretaria} : s
            );
        case actionTypes.REMOVE_SECRETARIA:
            return state.filter(s => s.id !== action.id);
        default:
            return state;
    }
};

const secretariasSelector = (state = [], action) => {
    switch (action.type) {
        case actionTypes.FETCH_SECRETARIAS_SELECTOR:
            return action.secretariasSelector;
        default:
            return state;
    }
};

const salarioSecretariasReport = (state = [], action) => {
    switch (action.type) {
        case actionTypes.FETCH_SALARIO_SECRETARIA:
            return action.salarioSecretariasReport;
        default:
            return state;
    }
};

export const getSecretarias = state => state.secretarias.secretarias;
export const getSecretariasSelector = state => state.secretarias.secretariasSelector;
export const getSalarioSecretariasReport = state => state.secretarias.salarioSecretariasReport;

export default combineReducers({
    secretarias,
    secretariasSelector,
    salarioSecretariasReport
});
