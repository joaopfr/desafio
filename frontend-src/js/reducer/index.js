import { combineReducers } from 'redux';
import funcionarios, * as fromFuncionarios from './funcionarios';
import secretarias, * as fromSecretarias from './secretarias';
import usuarios, * as fromUsuarios from './usuarios';
import fetching, * as fromFetching from './fetching';
import user, * as fromUser from './user';
import validation, * as fromValidations from './validation';
import mediaDocumentation, * as fromMediaDocumentation from './mediaDocumentation';

const reducer = combineReducers({
    funcionarios,
    secretarias,
    usuarios,
    fetching,
    user,
    validation,
    mediaDocumentation
});

export const getFuncionarios = state => fromFuncionarios.getFuncionarios(state);
export const getFuncionariosPeriodoAdmissaoExoneracao = state =>
    fromFuncionarios.getFuncionariosPeriodoAdmissaoExoneracao(state);

export const getSecretarias = state => fromSecretarias.getSecretarias(state);
export const getSecretariasSelector = state => fromSecretarias.getSecretariasSelector(state);
export const getSalarioSecretariasReport = state => fromSecretarias.getSalarioSecretariasReport(state);

export const getHasFetchingError = state => fromFetching.getHasFetchingError(state);
export const getIsLoading = state => fromFetching.getIsLoading(state);
export const getError = state => fromFetching.getError(state);

export const getValidationEntity = state => fromValidations.getValidationEntity(state);
export const getValidationErrors = state => fromValidations.getValidationErrors (state);
export const getHasValidationErrors = state => fromValidations.getHasValidationErrors(state);

export const getIsAuthenticated = state => fromUser.getIsAuthenticated(state);
export const getUser = state => fromUser.getUser(state);

export const getUsuarios = state => fromUsuarios.getUsuarios(state);

export const getHasStartedMediaDocumentationDownload = state => fromMediaDocumentation.getHasStartedMediaDocumentationDownload(state);
export const getMediaDocumentation = state => fromMediaDocumentation.getMediaDocumentation(state);

export default reducer;