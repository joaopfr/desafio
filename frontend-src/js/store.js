import { applyMiddleware, compose, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import reducer from './reducer';

const store = () => {
    const middlewares  = [
        thunk
    ];

    const enhancer = compose(
        applyMiddleware(...middlewares),
        composeWithDevTools()
    );

    return createStore(reducer, enhancer);
};

export default store;