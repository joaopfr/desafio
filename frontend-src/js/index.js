import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import CssBaseline from '@material-ui/core/CssBaseline';
import App from './Components/index';
import store from './store';
import 'typeface-roboto';

const Root = () => (
    <Provider store={store()}>
        <CssBaseline/>
        <App/>
    </Provider>
);

ReactDOM.render(<Root/>, document.getElementById('main'));
