const USUARIOS_URL = '/api/usuarios';
const NETWORK_REQUEST_ERROR_MESSAGE = 'Não foi possível acessar o servidor. Verifique sua conexão e tente novamente mais tarde.';

export const fetchUsuarios = token => {
    const requestOptions = {
        method: 'GET',
        headers: new Headers({
            'Authorization': `Bearer ${token}`
        })
    };

    return fetch(USUARIOS_URL, requestOptions).then(
        response => response.json().then(data => {
            if (response.ok === false) {
                throw data;
            }

            return data;
        })
    ).then(
        d => d,
        e => typeof(e.status) === 'undefined' ? Promise.resolve({
            status: 'NETWORK_ERROR',
            message: NETWORK_REQUEST_ERROR_MESSAGE
        }) : e
    );
};

export const addUsuario = ({ id, ...novoUsuarioData }, token) => {
    const requestOptions = {
        method: 'POST',
        headers: new Headers({
            'Authorization' : `Bearer ${token}`,
            'Content-Type': 'application/json'
        }),
        body: JSON.stringify(novoUsuarioData)
    };

    return fetch(USUARIOS_URL, requestOptions).then(
        response => response.json().then(data => {
            if (response.ok === false) {
                throw data;
            }

            return data;
        })
    ).then(
        d => d,
        e => typeof(e.status) === 'undefined' ? Promise.resolve({
            status: 'NETWORK_ERROR',
            message: NETWORK_REQUEST_ERROR_MESSAGE
        }) : e
    );
};

export const editUsuario = ({ id, ...usuarioData }, token) => {
    const requestOptions = {
        method: 'PUT',
        headers: new Headers({
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }),
        body: JSON.stringify(usuarioData)
    };

    const EDIT_USUARIO_URL = `${USUARIOS_URL}/${id}`;

    return fetch(EDIT_USUARIO_URL, requestOptions).then(
        response => response.json().then(data => {
            if (response.ok === false) {
                throw data;
            }

            return data;
        })
    ).then(
        d => d,
        e => typeof(e.status) === 'undefined' ? Promise.resolve({
            status: 'NETWORK_ERROR',
            message: NETWORK_REQUEST_ERROR_MESSAGE
        }) : e
    );
};

export const removeUsuario = (id, token) => {
    const requestOptions = {
        method: 'DELETE',
        headers: new Headers({
            'Authorization': `Bearer ${token}`
        })
    };

    const REMOVE_USUARIO_URL = `${USUARIOS_URL}/${id}`;

    return fetch(REMOVE_USUARIO_URL, requestOptions).then(
        response => response.json().then(data => {
            if (response.ok === false) {
                throw data;
            }

            return data;
        })
    ).then(
        d => d,
        e => typeof(e.status) === 'undefined' ? Promise.resolve({
            status: 'NETWORK_ERROR',
            message: NETWORK_REQUEST_ERROR_MESSAGE
        }) : e
    );
};