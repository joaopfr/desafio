const MEDIA_DOCUMENTATION_URL = '/api/media/documentacao';
const NETWORK_REQUEST_ERROR_MESSAGE = 'Não foi possível acessar o servidor. Verifique sua conexão e tente novamente mais tarde.';

export const getMediaDocumentation = (uuid, token) => {
    const requestOptions = {
        method: 'GET',
        headers: new Headers({
            'Authorization': `Bearer ${token}`
        })
    };

    const getMediaDocumentationUrl = `${MEDIA_DOCUMENTATION_URL}/${uuid}`;

    return fetch(getMediaDocumentationUrl, requestOptions).then(
        response => response.arrayBuffer().then(data => {
            if (response.ok === false) {
                throw data;
            }

            return {
                status: "OK",
                mediaDocumentation: {
                    uuid,
                    data
                }
            };
        })
    ).then(
        d => d,
        e => typeof(e.status) === 'undefined' ? Promise.resolve({
            status: 'NETWORK_ERROR',
            message: NETWORK_REQUEST_ERROR_MESSAGE
        }) : e
    );
};