const SECRETARIAS_URL = '/api/secretarias';
const NETWORK_REQUEST_ERROR_MESSAGE = 'Não foi possível acessar o servidor. Verifique sua conexão e tente novamente mais tarde.';

export const fetchSecretarias = token => {
    const requestOptions = {
        method: 'GET',
        headers: new Headers({
            'Authorization': `Bearer ${token}`
        })
    };

    return fetch(SECRETARIAS_URL, requestOptions).then(response => response.json().then(data => {
        if (response.ok === false) {
            throw data;
        }

        return data;
    })).then(
        d => d,
        e => typeof(estatus) === 'undefined' ? Promise.resolve({
            status: 'NETWORK_ERROR',
            message: NETWORK_REQUEST_ERROR_MESSAGE
        }) : e
    );
};

export const fetchSecretariasSelector = token => {
    const requestOptions = {
        method: 'GET',
        headers: new Headers({
            'Authorization': `Bearer ${token}`
        })
    };

    const secretariasSelectorUrl = `${SECRETARIAS_URL}/selector`;

    return fetch(secretariasSelectorUrl, requestOptions).then(response => response.json().then(data => {
        if (response.ok === false) {
            throw data;
        }

        return data;
    })).then(
        d => d,
        e => typeof(estatus) === 'undefined' ? Promise.resolve({
            status: 'NETWORK_ERROR',
            message: NETWORK_REQUEST_ERROR_MESSAGE
        }) : e
    );
};

export const addSecretaria = (novaSecretaria, token) => {
    const requestOptions = {
        method: 'POST',
        headers: new Headers({
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }),
        body: JSON.stringify(novaSecretaria)
    };

    return fetch(SECRETARIAS_URL, requestOptions).then(response => response.json().then(data => {
        if (response.ok === false) {
            throw data;
        }

        return data;
    })).then(
        d => d,
        e => typeof(e.status) === 'undefined' ? Promise.resolve({
            status: 'NETWORK_ERROR',
            message: NETWORK_REQUEST_ERROR_MESSAGE
        }) : e
    )
};

export const editSecretaria = ({ id: secretariaId, ...secretariaData }, token) => {
    const requestOptions = {
        method: 'PUT',
        headers: new Headers({
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }),
        body: JSON.stringify(secretariaData)
    };

    const editUrl = `${SECRETARIAS_URL}/${secretariaId}`;

    return fetch(editUrl, requestOptions).then(response => response.json().then(data => {
        if (response.ok === false) {
            throw data;
        }

        return data;
    })).then(
        d => d,
        e => typeof(e.status) === 'undefined' ? Promise.resolve({
            status: 'NETWORK_ERROR',
            message: NETWORK_REQUEST_ERROR_MESSAGE
        }) : e
    );
};

export const removeSecretaria = (id, token) => {
    const requestOptions = {
        method: 'DELETE',
        headers: new Headers({
            'Authorization': `Bearer ${token}`
        })
    };

    const removeUrl = `${SECRETARIAS_URL}/${id}`;

    return fetch(removeUrl, requestOptions).then(response => response.json().then(data => {
        if (response.ok === false) {
            throw data;
        }

        return data;
    })).then(
        d => d,
        e => typeof(e.status) === 'undefined' ? Promise.resolve({
            status: 'NETWORK_ERROR',
            message: NETWORK_REQUEST_ERROR_MESSAGE
        }) : e
    );
};

export const fetchSalarioSecretariasReport = token => {
    const requestOptions = {
        method: 'GET',
        headers: new Headers({
            'Authorization': `Bearer ${token}`
        })
    };

    const secretariaSalarioUrl = `${SECRETARIAS_URL}/relatorio-salario`;

    return fetch(secretariaSalarioUrl, requestOptions).then(response =>
        response.json().then(data => {
            if (response.ok === false) {
                throw data;
            }

            return data;
        })
    ).then(
        d => d,
        e => typeof(e.status) === 'undefined' ? Promise.resolve({
            status: 'NETWORK_ERROR',
            message: NETWORK_REQUEST_ERROR_MESSAGE
        }) : e
    );
};