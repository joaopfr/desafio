const FUNCIONARIOS_URL = '/api/funcionarios';
const DOCUMENTATION_URL = '/api/media/documentacao';
const NETWORK_REQUEST_ERROR_MESSAGE = 'Não foi possível acessar o servidor. Verifique sua conexão e tente novamente mais tarde.';

export const fetchFuncionarios = token => {
    const requestOptions = {
        method: 'GET',
        headers: new Headers({
            'Authorization': `Bearer ${token}`
        })
    };

    return fetch(FUNCIONARIOS_URL, requestOptions).then(response => {
        return response.json().then(data => {
            if (response.ok === false) {
                throw data;
            }

            return data;
        });
    }).then(
        d => d,
        e => typeof(e.status) === 'undefined' ? Promise.resolve({
            status: 'NETWORK_ERROR',
            message: NETWORK_REQUEST_ERROR_MESSAGE
        }) : e
    );
};

export const addFuncionario = async ({ id, ...novoFuncionario }, token) => {
    novoFuncionario.documentationMedias = novoFuncionario.documentationMedias.filter(
        docFile => docFile.status === 'ADDED'
    );

    const documentationsRequestOptions = novoFuncionario.documentationMedias.map(docFile => ({
        method: 'POST',
        headers: new Headers({
            'Content-Type': docFile.type,
            'Content-Length': docFile.size,
            'Content-Name': docFile.name,
            'Authorization': `Bearer ${token}`
        }),
        body: docFile
    }));

    const documentationUuids = Array(documentationsRequestOptions.length);
    const documentationUploadExceptions = [];

    const documentationFetches = documentationsRequestOptions.map((doc, index) =>
        fetch(DOCUMENTATION_URL, doc).then(
            response => response.json().then(data => {
                if (response.ok === false) {
                    throw data;
                }
                documentationUuids[index] = data.uuid;
            })
        ).catch(e => {
            if (typeof(e.status) === 'undefined') {
                documentationUploadExceptions.push(Promise.resolve({
                    status: 'NETWORK_ERROR',
                    message: NETWORK_REQUEST_ERROR_MESSAGE
                }));
            } else if (e.status === 'VALIDATION_ERROR') {
                documentationUploadExceptions.push(Promise.resolve({
                    status: e.status,
                    message: e.message,
                    errors: {
                        'documentationMedias': e.errors
                    }
                }));
            }
        })
    );

    if (documentationFetches.length > 0) {
        await Promise.all(documentationFetches);
        if (documentationUploadExceptions.length > 0) {
            return documentationUploadExceptions[0];
        }
    }

    novoFuncionario.documentationMedias = documentationUuids.map((uuid, index) => ({
        uuid,
        name: novoFuncionario.documentationMedias[index].name,
        type: novoFuncionario.documentationMedias[index].type
    }));

    const funcionarioRequestOptions = {
        method: 'POST',
        headers: new Headers({
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }),
        body: JSON.stringify(novoFuncionario)
    };

    return fetch(FUNCIONARIOS_URL, funcionarioRequestOptions).then( response => {
        return response.json().then(data => {
            if (response.ok === false) {
                throw data;
            }

            return data;
        });
    }).then(
        d => d,
        e => typeof(e.status) === 'undefined' ? Promise.resolve({
            status: 'NETWORK_ERROR',
            message: NETWORK_REQUEST_ERROR_MESSAGE
        }) : e
    );
};

export const editFuncionario = async ({ id: funcionarioId, ...funcionarioData }, token) => {
    const addDocumentationMedias = funcionarioData.documentationMedias.filter(
        docFile => docFile.status === 'ADDED'
    );

    const removeDocumentationMedias = funcionarioData.documentationMedias.filter(
        f => (typeof(f.uuid) !== 'undefined' && f.status === 'REMOVED')
    );

    const addDocumentationsRequestOptions = addDocumentationMedias.map(docFile => ({
        method: 'POST',
        headers: new Headers({
            'Content-Type': docFile.type,
            'Content-Length': docFile.size,
            'Content-Name': docFile.name,
            'Authorization': `Bearer ${token}`
        }),
        body: docFile
    }));

    const addedDocumentationUuids = Array(addDocumentationsRequestOptions.length);
    const addDocumentationUploadExceptions = [];

    const addDocumentationFetches = addDocumentationsRequestOptions.map((docRequest, index) =>
        fetch(DOCUMENTATION_URL, docRequest).then(
            response => response.json().then(data => {
                if (response.ok === false) {
                    throw data;
                }
                addedDocumentationUuids[index] = data.uuid;
            })
        ).catch(e => {
            if (typeof(e.status) === 'undefined') {
                addDocumentationUploadExceptions.push(Promise.resolve({
                    status: 'NETWORK_ERROR',
                    message: NETWORK_REQUEST_ERROR_MESSAGE
                }));
            } else if (e.status === 'VALIDATION_ERROR') {
                addDocumentationUploadExceptions.push(Promise.resolve({
                    status: e.status,
                    message: e.message,
                    errors: {
                        'documentationMedias': e.errors
                    }
                }));
            }
        })
    );

    if (addDocumentationFetches.length > 0) {
        await Promise.all(addDocumentationFetches);
        if (addDocumentationUploadExceptions.length > 0) {
            return addDocumentationUploadExceptions[0];
        }
    }

    const addedDocumentationMedias = addedDocumentationUuids.map((uuid, index) => ({
        uuid,
        name: addDocumentationMedias[index].name,
        type: addDocumentationMedias[index].type
    }));

    const removeDocumentationsRequestOptions = removeDocumentationMedias.map(docFile => ({
        url: `${DOCUMENTATION_URL}/${docFile.uuid}`,
        requestOpt: {
            method: 'DELETE',
            headers: new Headers({
                'Authorization': `Bearer ${token}`
            }),
        }
    }));

    const removeDocumentationDeleteExceptions = [];

    const removeDocumentationFetches = removeDocumentationsRequestOptions.map((docRequest, index) =>
        fetch(docRequest.url, docRequest.requestOpt).then(
            response => response.json().then(data => {
                if (response.ok === false) {
                    throw data;
                }

                return data;
            })
        ).catch(e => {
            // ignore MEDIA_NOT_FOUND status.
            if (typeof(e.status) === 'undefined') {
                removeDocumentationDeleteExceptions.push(Promise.resolve({
                    status: 'NETWORK_ERROR',
                    message: NETWORK_REQUEST_ERROR_MESSAGE
                }));
            }
        })
    );

    if (removeDocumentationFetches.length > 0) {
        await Promise.all(removeDocumentationFetches);
        if (removeDocumentationDeleteExceptions.length > 0) {
            return removeDocumentationDeleteExceptions[0];
        }
    }

    funcionarioData.documentationMedias = [
        ...addedDocumentationMedias,
        ...funcionarioData.documentationMedias.filter(f => typeof(f.uuid) !== 'undefined' && f.status !== 'REMOVED')
    ];

    const requestOptions = {
        method: 'PUT',
        headers: new Headers({
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }),
        body: JSON.stringify(funcionarioData)
    };

    const putFuncionarioUrl = `${FUNCIONARIOS_URL}/${funcionarioId}`;

    return fetch(putFuncionarioUrl, requestOptions).then(response => {
        return response.json().then(data => {
            if (response.ok === false) {
                throw data;
            }

            return data;
        });
    }).then(
        d => d,
        e => typeof(e.status) === 'undefined' ? Promise.resolve({
            status: 'NETWORK_ERROR',
            message: NETWORK_REQUEST_ERROR_MESSAGE
        }) : e
    );
};

export const removeFuncionario = (funcionarioId, token) => {
    const requestOptions = {
        method: 'DELETE',
        headers: new Headers({
            'Authorization': `Bearer ${token}`
        })
    };

    const deleteFuncionarioUrl = `${FUNCIONARIOS_URL}/${funcionarioId}`;

    return fetch(deleteFuncionarioUrl, requestOptions).then(response =>
        response.json().then(data => {
            if (response.ok === false) {
                throw data;
            }

            return data;
        }).then(
            d => d,
            e => typeof(e.status) === 'undefined' ?
                Promise.resolve({
                    status: 'NETWORK_ERROR',
                    message: NETWORK_REQUEST_ERROR_MESSAGE
                }) : e
        )
    );
};

export const fetchFuncionarioPeriodoAdmissaoExoneracao = (funcionarioPeriodoFormValues, token) => {
    const requestOptions = {
        method: 'GET',
        headers: new Headers({
            'Authorization': `Bearer ${token}`
        })
    };

    const {
        dateBegin,
        dateEnd,
        isAdmissao
    } = funcionarioPeriodoFormValues;

    const queryString = `dateBegin=${dateBegin}&dateEnd=${dateEnd}&isAdmissao=${isAdmissao}`;
    const funcionarioPeriodoFormValuesUrl = `${FUNCIONARIOS_URL}/relatorio-admissao-exoneracao?${queryString}`;

    return fetch(funcionarioPeriodoFormValuesUrl, requestOptions).then(response =>
        response.json().then(data => {
            if (response.ok === false) {
                throw data;
            }

            return data;
        }).then(
            d => d,
            e => typeof(e.status) === 'undefined' ?
                Promise.resolve({
                    status: 'NETWORK_ERROR',
                    message: NETWORK_REQUEST_ERROR_MESSAGE
                }) : e
        )
    );
};