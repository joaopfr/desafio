const SIGNIN_URL = '/api/login';
const NETWORK_REQUEST_ERROR_MESSAGE = 'Não foi possível acessar o servidor. Verifique sua conexão e tente novamente mais tarde.';

const authErrorSchema = {
    code: 'number',
    message: 'string'
};

const authErrorObject = {
    status: 'AUTH_ERROR',
    message: 'O email ou a senha não conferem com nenhum usuário cadastrado'
};

const isAuthError = data =>
    typeof(data.code) === authErrorSchema.code &&
    typeof(data.message) === authErrorSchema.message;

export const signin = credentials => {
    const user = {
        username: credentials.email,
        password: credentials.senha
    };

    const requestOptions = {
        method: 'POST',
        'Content-Type': 'application/json',
        body: JSON.stringify(user)
    };

    return fetch(SIGNIN_URL, requestOptions).then(response =>
        response.json().then(data => {
            if (response.ok === false) {
                if (isAuthError(data) === true) {
                    throw authErrorObject;
                }

                throw data;
            }

            return {
                status: 'OK',
                token: data.token
            };
        })
    ).then(
        d => d,
        e => typeof(e.status) === 'undefined' ? Promise.resolve({
            status: 'NETWORK_ERROR',
            message: NETWORK_REQUEST_ERROR_MESSAGE
        }) : e
    );
};