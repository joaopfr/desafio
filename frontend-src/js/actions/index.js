import * as actionTypes from './types';
import * as funcionariosApi from '../api/funcionariosApi';
import * as secretariasApi from '../api/secretariasApi';
import * as usuariosApi from '../api/usuariosApi';
import * as userApi from '../api/userApi';
import * as mediaDocumentationApi from '../api/mediaDocumentationApi';

export const fetchFuncionarios = user => dispatch => {
    dispatch({
        type: actionTypes.REMOTE_REQUEST
    });

    return funcionariosApi.fetchFuncionarios(user.token).then(funcionariosResp => {
        if (funcionariosResp.status === 'OK') {
            dispatch({
                type: actionTypes.REMOTE_REQUEST_SUCCESS
            });
            dispatch({
                type: actionTypes.FETCH_FUNCIONARIOS,
                funcionarios: funcionariosResp.funcionarios
            });
        } else {
            dispatch({
                type: actionTypes.REMOTE_REQUEST_ERROR,
                message: funcionariosResp.message
            });
        }
    });
};

export const addFuncionario = (newFuncionario, user) => dispatch => {
    dispatch({
        type: actionTypes.REMOTE_REQUEST
    });

    return funcionariosApi.addFuncionario(newFuncionario, user.token).then(
        funcionarioResp => new Promise((resolve, reject) => {
            if (funcionarioResp.status === 'OK') {
                dispatch({
                    type: actionTypes.REMOTE_REQUEST_SUCCESS
                });
                dispatch({
                   type: actionTypes.ADD_FUNCIONARIO,
                   funcionario: funcionarioResp.funcionario
                });
                resolve();
            } else if (funcionarioResp.status === 'VALIDATION_ERROR') {
                dispatch({
                    type: actionTypes.REMOTE_VALIDATION_ERROR,
                    entity: newFuncionario,
                    errors: funcionarioResp.errors
                });
            } else {
                dispatch({
                    type: actionTypes.REMOTE_REQUEST_ERROR,
                    message: funcionarioResp.message
                });
            }
        })
    );
};

export const editFuncionario = (funcionario, user) => dispatch => {
    dispatch({
        type: actionTypes.REMOTE_REQUEST
    });

    return funcionariosApi.editFuncionario(funcionario, user.token).then(
        funcionarioResp => new Promise((resolve, reject) => {
            if (funcionarioResp.status === 'OK') {
                dispatch({
                    type: actionTypes.REMOTE_REQUEST_SUCCESS
                });
                dispatch({
                    type: actionTypes.EDIT_FUNCIONARIO,
                    funcionario: funcionarioResp.funcionario
                });
                resolve();
            } else if (funcionarioResp.status === 'VALIDATION_ERROR') {
                dispatch({
                    type: actionTypes.REMOTE_VALIDATION_ERROR,
                    entity: funcionario,
                    errors: funcionarioResp.errors
                });
            } else {
                dispatch({
                    type: actionTypes.REMOTE_REQUEST_ERROR,
                    message: funcionarioResp.message
                })
            }
        })
    );
};

export const removeFuncionario = (id, user) => dispatch => {
    dispatch({
        type: actionTypes.REMOTE_REQUEST,
    });

    return funcionariosApi.removeFuncionario(id, user.token).then(funcionarioResp =>
        new Promise((resolve, reject) => {
            if (funcionarioResp.status === 'OK') {
                dispatch({
                    type: actionTypes.REMOTE_REQUEST_SUCCESS
                });
                dispatch({
                    type: actionTypes.REMOVE_FUNCIONARIO
                });
                resolve();
            } else {
                dispatch({
                    type: actionTypes.REMOTE_REQUEST_ERROR,
                    message: funcionarioResp.message
                });
                reject();
            }
    }));
};

export const fetchSecretarias = user => dispatch => {
    dispatch({
        type: actionTypes.REMOTE_REQUEST
    });

    return secretariasApi.fetchSecretarias(user.token).then(secretariasResp => {
        if (secretariasResp.status === 'OK') {
            dispatch({
                type: actionTypes.REMOTE_REQUEST_SUCCESS
            });
            dispatch({
                type: actionTypes.FETCH_SECRETARIAS,
                secretarias: secretariasResp.secretarias
            });
        } else {
            dispatch({
                type: actionTypes.REMOTE_REQUEST_ERROR,
                message: secretariasResp.message
            });
        }
    });
};

export const fetchFuncionariosPeriodoAdmissaoExoneracao = (user, funcionarioPeriodoFormValues) => dispatch => {
    dispatch({
        type: actionTypes.REMOTE_REQUEST
    });

    return funcionariosApi.fetchFuncionarioPeriodoAdmissaoExoneracao(funcionarioPeriodoFormValues, user.token).then(
        funcionarioPeriodoAdmissaoExoneracaoResp => {
            if (funcionarioPeriodoAdmissaoExoneracaoResp.status === 'OK') {
                dispatch({
                    type: actionTypes.REMOTE_REQUEST_SUCCESS
                });
                dispatch({
                    type: actionTypes.FETCH_FUNCIONARIOS_PERIODO_ADMISSAO_EXONERACAO,
                    funcionariosPeriodoAdmissaoExoneracao:
                        funcionarioPeriodoAdmissaoExoneracaoResp.funcionariosPeriodoAdmissaoExoneracao
                });
            } else if (funcionarioPeriodoAdmissaoExoneracaoResp.status === 'VALIDATION_ERROR') {
                dispatch({
                    type: actionTypes.REMOTE_VALIDATION_ERROR,
                    entity: funcionarioPeriodoFormValues,
                    errors: funcionarioPeriodoAdmissaoExoneracaoResp.errors
                });
            } else {
                dispatch({
                    type: actionTypes.REMOTE_REQUEST_ERROR,
                    message: secretariasResp.message
                });
            }
        }
    )
};

export const fetchSecretariasSelector = user => dispatch => {
    dispatch({
        type: actionTypes.REMOTE_REQUEST
    });

    return secretariasApi.fetchSecretariasSelector(user.token).then(secretariasResp => {
        if (secretariasResp.status === 'OK') {
            dispatch({
                type: actionTypes.REMOTE_REQUEST_SUCCESS
            });
            dispatch({
                type: actionTypes.FETCH_SECRETARIAS_SELECTOR,
                secretariasSelector: secretariasResp.secretarias
            });
        } else {
            dispatch({
                type: actionTypes.REMOTE_REQUEST_ERROR,
                message: secretariasResp.message
            });
        }
    });
};

export const addSecretaria = ({ nome }, user) => dispatch => {
    dispatch({
        type: actionTypes.REMOTE_REQUEST
    });

    return secretariasApi.addSecretaria({
        nome
    }, user.token).then(secretariaResp => new Promise((resolve, reject) => {
        if (secretariaResp.status === 'OK') {
            dispatch({
                type: actionTypes.REMOTE_REQUEST_SUCCESS
            });
            dispatch({
                type: actionTypes.ADD_SECRETARIA,
                secretaria: secretariaResp.secretaria
            });
            resolve();
        } else if (secretariaResp.status === 'VALIDATION_ERROR') {
            dispatch({
                type: actionTypes.REMOTE_VALIDATION_ERROR,
                entity: {
                    nome
                },
                errors: secretariaResp.errors
            });
        } else {
            dispatch({
                type: actionTypes.REMOTE_REQUEST_ERROR,
                message: secretariaResp.message
            });
        }
    }));
};

export const editSecretaria = (secretaria, user) =>  dispatch => {
    dispatch({
        type: actionTypes.REMOTE_REQUEST
    });

    return secretariasApi.editSecretaria(secretaria, user.token).then(secretariaResp =>
        new Promise((resolve, reject) => {
            if (secretariaResp.status === 'OK') {
                dispatch({
                    type: actionTypes.REMOTE_REQUEST_SUCCESS
                });

                dispatch({
                    type: actionTypes.EDIT_SECRETARIA,
                    secretaria: secretariaResp.secretaria
                });
                resolve();
            } else if (secretariaResp.status === 'VALIDATION_ERROR') {
                dispatch({
                    type: actionTypes.REMOTE_VALIDATION_ERROR,
                    entity: secretaria,
                    errors: secretariaResp.errors
                });
            } else {
                dispatch({
                    type: actionTypes.REMOTE_REQUEST_ERROR,
                    message: secretariaResp.message
                });
            }
        })
    );
};

export const removeSecretaria = (id, user) => dispatch => {
    dispatch({
        type: actionTypes.REMOTE_REQUEST
    });

    return secretariasApi.removeSecretaria(id, user.token).then(secretariaResp =>
        new Promise((resolve, reject) => {
            if (secretariaResp.status === 'OK') {
                dispatch({
                    type: actionTypes.REMOTE_REQUEST_SUCCESS
                });

                dispatch({
                    type: actionTypes.REMOVE_SECRETARIA,
                    id
                });
                resolve();
            } else {
                dispatch({
                    type: actionTypes.REMOTE_REQUEST_ERROR,
                    message: secretariaResp.message
                });
                reject();
            }
        })
    );
};

export const fetchSalarioSecretariasReport = user => dispatch => {
    dispatch({
        type: actionTypes.REMOTE_REQUEST
    });

    return secretariasApi.fetchSalarioSecretariasReport(user.token).then(secretariasResp => {
        if (secretariasResp.status === 'OK') {
            dispatch({
                type: actionTypes.REMOTE_REQUEST_SUCCESS
            });
            dispatch({
                type: actionTypes.FETCH_SALARIO_SECRETARIA,
                salarioSecretariasReport: secretariasResp.salarioSecretariasReport
            });
        } else {
            dispatch({
                type: actionTypes.REMOTE_REQUEST_ERROR,
                message: secretariasResp.message
            });
        }
    });

};

export const clearValidationErrors = () => dispatch => Promise.resolve(dispatch({
    type: actionTypes.CLEAR_VALIDATION_ERROR
}));

export const clearRemoteRequestErrors = () => dispatch => Promise.resolve(dispatch({
    type: actionTypes.CLEAR_REMOTE_REQUEST_ERROR
}));

const parseJWT = token => {
    const base64Url = token.split('.')[1];
    const base64 = base64Url.replace(/-/g, '+')
        .replace(/_/g, '/');
    return JSON.parse(window.atob(base64));
};

export const userSignin = user => dispatch => {
    dispatch({
        type: actionTypes.REMOTE_REQUEST
    });

    return userApi.signin(user).then(userResp =>
        new Promise((resolve, reject) => {
            if (userResp.status === 'OK') {
                dispatch({
                    type: actionTypes.REMOTE_REQUEST_SUCCESS
                });

                const userJWT = parseJWT(userResp.token);
                const user = {
                    email: userJWT.username,
                    roles: userJWT.roles.join(''),
                    token: userResp.token
                };

                dispatch({
                    type: actionTypes.USER_SIGNIN,
                    user
                });

                resolve();
            } else if (userResp.status === 'AUTH_ERROR') {
                dispatch({
                    type: actionTypes.REMOTE_AUTH_ERROR,
                    message: userResp.message
                });
            } else {
                dispatch({
                    type: actionTypes.REMOTE_REQUEST_ERROR,
                    message: userResp.message
                });
                reject();
            }
        })
    );
};

export const userLogout = () => dispatch => Promise.resolve({
    type: actionTypes.USER_LOGOUT
});

export const fetchUsuarios = user => dispatch => {
    dispatch({
        type: actionTypes.REMOTE_REQUEST
    });

    return usuariosApi.fetchUsuarios(user.token).then(usuariosResp => {
        if (usuariosResp.status === 'OK') {
            dispatch({
                type: actionTypes.REMOTE_REQUEST_SUCCESS
            });
            dispatch({
                type: actionTypes.FETCH_USUARIOS,
                usuarios: usuariosResp.usuarios.map(u => ({
                    ...u,
                    roles: u.roles.join('')
                }))
            });
        } else {
            dispatch({
                type: actionTypes.REMOTE_REQUEST_ERROR,
                message: usuariosResp.message
            });
        }
    });
};

export const addUsuario = (novoUsuario, user) => dispatch => {
    dispatch({
        type: actionTypes.REMOTE_REQUEST
    });

    const apiNovoUsuario = {
        ...novoUsuario,
        roles: [novoUsuario.roles]
    };

    return usuariosApi.addUsuario(apiNovoUsuario, user.token).then(
        usuariosResp => new Promise((resolve, reject)=> {
            if (usuariosResp.status === 'OK') {
                dispatch({
                    type: actionTypes.REMOTE_REQUEST_SUCCESS
                });
                dispatch({
                    type: actionTypes.ADD_USUARIO,
                    usuario: {
                        ...usuariosResp.usuario,
                        roles: usuariosResp.usuario.roles.join('') // Current only one role is used for each user.
                    }
                });
                resolve();
            } else if (usuariosResp.status === 'VALIDATION_ERROR') {
                dispatch({
                    type: actionTypes.REMOTE_VALIDATION_ERROR,
                    entity: novoUsuario,
                    errors: usuariosResp.errors
                });
            } else {
                dispatch({
                    type: actionTypes.REMOTE_REQUEST_ERROR,
                    message: usuariosResp.message
                });
            }
        })
    );
};

export const editUsuario = (usuario, user) => dispatch => {
    dispatch({
        type: actionTypes.REMOTE_REQUEST
    });

    const apiUsuario = {
        ...usuario,
        roles: [usuario.roles]
    };

    return usuariosApi.editUsuario(apiUsuario, user.token).then(
        usuariosResp => new Promise((resolve, reject) => {
            if (usuariosResp.status === 'OK') {
                dispatch({
                    type: actionTypes.REMOTE_REQUEST_SUCCESS
                });
                console.log('#$#@$', usuariosResp);
                dispatch({
                    type: actionTypes.EDIT_USUARIO,
                    usuario: {
                        ...usuariosResp.usuario,
                        roles: usuariosResp.usuario.roles.join('')
                    }
                });
                resolve();
            } else if (usuariosResp.status === 'VALIDATION_ERROR') {
                dispatch({
                    type: actionTypes.REMOTE_VALIDATION_ERROR,
                    entity: usuario,
                    errors: usuariosResp.errors
                })
            } else {
                dispatch({
                    type: actionTypes.REMOTE_REQUEST_ERROR,
                    message: usuariosResp.message
                });
            }
        })
    );
};

export const removeUsuario = (id, user) => dispatch => {
    dispatch({
        type: actionTypes.REMOTE_REQUEST
    });

    return usuariosApi.removeUsuario(id, user.token).then(
        usuariosResp => new Promise((resolve, reject) => {
            if (usuariosResp.status === 'OK') {
                dispatch({
                    type: actionTypes.REMOTE_REQUEST_SUCCESS
                });
                dispatch({
                    type: actionTypes.REMOVE_USUARIO,
                    id
                });
                resolve();
            } else {
                dispatch({
                    type: actionTypes.REMOTE_REQUEST_ERROR,
                    message: usuariosResp.message
                });
                reject();
            }
        })
    );
};

export const startDownloadMediaDocumentation = (uuid, user) => dispatch => {
    dispatch({
        type: actionTypes.REMOTE_REQUEST
    });

    return mediaDocumentationApi.getMediaDocumentation(uuid, user.token).then(
        mediaDocumentationResp => new Promise((resolve, reject) => {
            if (mediaDocumentationResp.status === 'OK') {
                dispatch({
                    type: actionTypes.REMOTE_REQUEST_SUCCESS
                });
                dispatch({
                    type: actionTypes.START_DOWNLOAD_MEDIA_DOCUMENTATION,
                    mediaDocumentation: mediaDocumentationResp.mediaDocumentation
                });
                resolve();
            } else {
                dispatch({
                    type: actionTypes.REMOTE_REQUEST_ERROR,
                    message: mediaDocumentationResp.message
                });
            }
        })
    )
};

export const stopDownloadMediaDocumentation = () => dispatch => Promise.resolve(dispatch({
    type: actionTypes.STOP_DOWNLOAD_MEDIA_DOCUMENTATION
}));