<?php

namespace App\Form;

use App\Entity\User;
use App\Utils\SecurityUtils;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email')
            ->add('password')
            ->add(
                'roles',
                ChoiceType::class, [
                    'choices' => [
                        SecurityUtils::OPERADOR => SecurityUtils::ROLE_OPERADOR,
                        SecurityUtils::GERENTE => SecurityUtils::ROLE_GERENTE,
                        SecurityUtils::ADMINISTRADOR => SecurityUtils::ROLE_ADMINISTRADOR,
                    ],
                    'multiple' => true
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
