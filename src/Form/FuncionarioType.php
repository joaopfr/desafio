<?php

namespace App\Form;

use App\Entity\Funcionario;
use App\Entity\FuncionarioStatus;
use App\Entity\Posse;
use App\Entity\Secretaria;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class FuncionarioType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nome')
            ->add('cpf')
            ->add('secretaria', EntityType::class, [
                'class' => Secretaria::class,
                'choice_value' => 'id'
            ])
            ->add('status', EntityType::class, [
                'class' => FuncionarioStatus::class,
                'choice_value' => 'status'
            ])
            ->add('posse', EntityType::class, [
                'class' => Posse::class,
                'choice_value' => 'tipo'
            ])
            ->add('salarioBase')
            ->add('gratificacao')
            ->add('desconto')
            ->add('dataAdmissao', DateType::class, [
                'widget' => 'single_text'
            ])
            ->add('dataExoneracao', DateType::class, [
                'widget' => 'single_text'
            ])
            ->add('documentationMedias', CollectionType::class, [
                'entry_type' => DocumentationMediaType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Funcionario::class,
        ]);
    }
}
