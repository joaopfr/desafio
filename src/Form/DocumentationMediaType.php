<?php

namespace App\Form;

use App\Entity\DocumentationMedia;
use App\Entity\Funcionario;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DocumentationMediaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('uuid')
            ->add('name')
            ->add('type')
            ->add('funcionario', EntityType::class, [
                'class' => Funcionario::class,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DocumentationMedia::class,
            'csrf_protection' => false
        ]);
    }
}
