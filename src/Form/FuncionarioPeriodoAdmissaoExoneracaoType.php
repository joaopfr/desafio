<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class FuncionarioPeriodoAdmissaoExoneracaoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateBegin', DateType::class, [
                'widget' => 'single_text',
                'constraints' => new NotNull(),
            ])
            ->add('dateEnd', DateType::class, [
                'widget' => 'single_text',
                'constraints' => [
                    new NotNull(),
                    new Callback([
                        'callback' => function ($object, ExecutionContextInterface $context, $payload) {
                            $dateBegin = $context->getRoot()->getData()['dateBegin'];
                            if ($object <= $dateBegin) {
                                $context
                                    ->buildViolation('A data final deve ser maior que a data inicial')
                                    ->addViolation()
                                ;
                            }
                        }
                    ])
                ],
            ])
            ->add('isAdmissao', ChoiceType::class, [
                'choices' => [
                    'true'=> 'true',
                    'false' => 'false',
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
