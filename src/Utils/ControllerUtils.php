<?php
/**
 * Created by PhpStorm.
 * User: joaopfr
 * Date: 3/7/19
 * Time: 12:39 PM
 */

namespace App\Utils;


use Symfony\Component\Form\FormInterface;

class ControllerUtils
{
    public static function getFormErrors(FormInterface $form)
    {
        $errors= [];
        foreach ($form->all() as $child)
        {
            $errors[$child->getName()] = [];
            foreach ($child->getErrors(true) as $error)
            {
                $errors[$child->getName()][] = $error->getMessage();
            }
        }

        return $errors;
    }
}