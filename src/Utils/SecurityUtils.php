<?php

namespace App\Utils;


class SecurityUtils
{
    const OPERADOR ='OPERADOR';
    const ROLE_OPERADOR = 'ROLE_OPERADOR';

    const GERENTE = 'GERENTE';
    const ROLE_GERENTE = 'ROLE_GERENTE';

    const ADMINISTRADOR = 'ADMINISTRADOR';
    const ROLE_ADMINISTRADOR = 'ROLE_ADMINISTRADOR';
}