<?php

namespace App\Repository;

use App\Entity\DocumentationMedia;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method DocumentationMedia|null find($id, $lockMode = null, $lockVersion = null)
 * @method DocumentationMedia|null findOneBy(array $criteria, array $orderBy = null)
 * @method DocumentationMedia[]    findAll()
 * @method DocumentationMedia[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DocumentationMediaRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DocumentationMedia::class);
    }

    // /**
    //  * @return DocumentationMedia[] Returns an array of DocumentationMedia objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DocumentationMedia
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
