<?php

namespace App\Repository;

use App\Entity\Funcionario;
use App\Entity\FuncionarioStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Funcionario|null find($id, $lockMode = null, $lockVersion = null)
 * @method Funcionario|null findOneBy(array $criteria, array $orderBy = null)
 * @method Funcionario[]    findAll()
 * @method Funcionario[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FuncionarioRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Funcionario::class);
    }

    public function funcionariosPeriodoAdmissaoExoneracao(\DateTime $dateBegin, \DateTime $dateEnd, bool $isAdmissao)
    {
        $dateAttr = $isAdmissao ? 'f.dataAdmissao' : 'f.dataExoneracao';

        $qb = $this->createQueryBuilder('f');
        $qb
            ->where($qb->expr()->isNotNull($dateAttr))
            ->andWhere($qb->expr()->between($dateAttr, ':dateBegin', ':dateEnd'));

        if ($isAdmissao === false) {
            $qb
                ->innerJoin('f.status', 'st')
                ->andWhere('st.status = :status')
                ->setParameter(':status', FuncionarioStatus::EXONERADO);
        }

        return $qb
            ->setParameter(':dateBegin', $dateBegin->format('Y-m-d'))
            ->setParameter(':dateEnd', $dateEnd->format('Y-m-d'))
            ->getQuery()
            ->getResult()
        ;
    }

    // /**
    //  * @return Funcionario[] Returns an array of Funcionario objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Funcionario
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
