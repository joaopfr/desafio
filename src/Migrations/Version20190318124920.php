<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190318124920 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE funcionario_status (id INT AUTO_INCREMENT NOT NULL, status VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE posse (id INT AUTO_INCREMENT NOT NULL, tipo VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql("ALTER TABLE funcionario_status ADD CONSTRAINT CHK_FUNCIONARIO_STATUS CHECK (status IN ('ATIVO', 'EXONERADO'))");
        $this->addSql("ALTER TABLE posse ADD CONSTRAINT CHK_POSSE CHECK (tipo IN ('COMISSIONADO', 'ESTATUTARIO'))");
        $this->addSql('ALTER TABLE funcionario ADD status_id INT DEFAULT NULL, ADD posse_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE funcionario ADD CONSTRAINT FK_7510A3CF6BF700BD FOREIGN KEY (status_id) REFERENCES funcionario_status (id)');
        $this->addSql('ALTER TABLE funcionario ADD CONSTRAINT FK_7510A3CF12B08C96 FOREIGN KEY (posse_id) REFERENCES posse (id)');
        $this->addSql('CREATE INDEX IDX_7510A3CF6BF700BD ON funcionario (status_id)');
        $this->addSql('CREATE INDEX IDX_7510A3CF12B08C96 ON funcionario (posse_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE funcionario DROP FOREIGN KEY FK_7510A3CF6BF700BD');
        $this->addSql('ALTER TABLE funcionario DROP FOREIGN KEY FK_7510A3CF12B08C96');
        $this->addSql('DROP TABLE funcionario_status');
        $this->addSql('DROP TABLE posse');
        $this->addSql('DROP INDEX IDX_7510A3CF6BF700BD ON funcionario');
        $this->addSql('DROP INDEX IDX_7510A3CF12B08C96 ON funcionario');
        $this->addSql('ALTER TABLE funcionario DROP status_id, DROP posse_id');
    }
}
