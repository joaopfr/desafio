<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Utils\ControllerUtils;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Class UserController
 * @package App\Controller
 * @IsGranted("ROLE_ADMINISTRADOR")
 */
class UserController extends AbstractFOSRestController
{
    private $errorResponse = [
        "status" => "EXCEPTION_ERROR",
        "message" => "Erro no servidor. Contate o administrador do sistema."
    ];

    /**
     * @Rest\Get("/api/usuarios", name="get_usuarios")
     * @View(serializerGroups={"users"})
     * @IsGranted("ROLE_ADMINISTRADOR")
     */
    public function getUsers()
    {
        try {
            $em = $this->getDoctrine()->getManager();
            $users = $em->getRepository(User::class)->findAll();
            return $this->view(
                [
                    "status" => "OK",
                    "usuarios" => $users,
                ],
                Response::HTTP_OK
            );
        } catch (Exception $e) {
            \Monolog\Handler\error_log($e->getMessage());
            return $this->view(
                $this->errorResponse,
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    /**
     * @Rest\Post("/api/usuarios", name="post_usuarios")
     * @View(serializerGroups={"users"})
     */
    public function postUser(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        try {
            $user = new User();
            $form = $this->createForm(
                UserType::class,
                $user,
                [
                    "csrf_protection" => false
                ]
            );

            $requestData = $request->request->all();
            $requestData["password"] = $passwordEncoder->encodePassword($user, $requestData["password"]);

            $form->submit($requestData);

            if ($form->isValid() === false) {
                $formErrors = ControllerUtils::getFormErrors($form);
                return $this->view(
                    [
                        "status" => "VALIDATION_ERROR",
                        "errors" => $formErrors,
                        "message" => "Erro de validação. O formulário deve ser preenchido corretamente"
                    ],
                    Response::HTTP_BAD_REQUEST
                );
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->view(
                [
                    "status" => "OK",
                    "usuario" => $user
                ],
                Response::HTTP_CREATED
            );
        } catch (Exception $e) {
            \Monolog\Handler\error_log($e->getMessage());
            return $this->view(
                $this->errorResponse,
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    /**
     * @Rest\Put("/api/usuarios/{id}", name="put_funcionarios")
     * @View(serializerGroups={"users"})
     */
    public function putUser(Request $request, User $user, UserPasswordEncoderInterface $passwordEncoder)
    {
        try {
            $form = $this->createForm(
                UserType::class,
                $user,
                [
                    "csrf_protection" => false,
                ]
            );
            $requestData = $request->request->all();

            $requestData["password"] = $passwordEncoder->encodePassword($user, $requestData["password"]);
            $form->submit($requestData);

            if ($form->isValid() === false) {
                $formErrors = ControllerUtils::getFormErrors($form);
                return $this->view(
                    [
                        "status" => "VALIDATION_ERROR",
                        "errors" => $formErrors,
                        "message" => "Erro de validação. O formulário deve ser preenchido corretamente"
                    ],
                    Response::HTTP_BAD_REQUEST
                );
            }

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->view(
                [
                    "status" => "OK",
                    "usuario" => $user
                ],
                Response::HTTP_OK
            );
        } catch (Exception $e) {
            \Monolog\Handler\error_log($e->getMessage());
            return $this->view(
                $this->errorResponse,
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    /**
     * @Rest\Delete("/api/usuarios/{id}", name="delete_usuarios")
     */
    public function deleteUser(User $user)
    {
        try {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();

            return $this->view(
                [
                    "status" => "OK",
                ],
                Response::HTTP_OK
            );
        } catch(Exception $e) {
            \Monolog\Handler\error_log($e->getMessage());
            return $this->view(
                $this->errorResponse,
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }
}
