<?php

namespace App\Controller;

use App\Entity\Secretaria;
use App\Form\SecretariaType;
use App\Repository\SecretariaRepository;
use App\Utils\ControllerUtils;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Class SecretariaController
 * @package App\Controller
 */
class SecretariaController extends AbstractFOSRestController
{
    private $errorResponse = [
        "status" => "EXCEPTION_ERROR",
        "message" => "Erro no servidor. Contate o administrador do sistema."
    ];

    /**
     * @Rest\Get("/api/secretarias", name="secretarias")
     * @View(serializerGroups={"secretarias"})
     * @IsGranted("ROLE_GERENTE")
     */
    public function getSecretarias()
    {
        try
        {
            $em = $this->getDoctrine()->getManager();
            $secretarias = $em->getRepository(Secretaria::class)->findAll();

            return $this->view(
                [
                    "status" => "OK",
                    "secretarias" => $secretarias
                ],
                Response::HTTP_OK
            );
        }
        catch (Exception $e)
        {
            \Monolog\Handler\error_log($e->getMessage());
            return $this->view(
                $this->errorResponse,
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    /**
     * @Rest\Get("/api/secretarias/selector", name="get_secretarias_selector")
     * @View(serializerGroups={"secretariasSelector"})
     * @IsGranted("ROLE_OPERADOR")
     */
    public function getSecretariasSelector()
    {
        return $this->getSecretarias();
    }

    /**
     * @Rest\Post("/api/secretarias", name="post_secretarias")
     * @View(serializerGroups={"secretarias"})
     * @IsGranted("ROLE_GERENTE")
     */
    public function postSecretaria(Request $request)
    {
        try
        {
            $secretaria = new Secretaria();
            $form = $this->createForm(SecretariaType::class, $secretaria, [
                "csrf_protection" => false
            ]);

            $form->submit($request->request->all());
            if ($form->isValid() === false) {
                $formErrors = ControllerUtils::getFormErrors($form);
                return $this->view(
                    [
                        "status" => "VALIDATION_ERROR",
                        "errors" => $formErrors,
                        "message" => "Erro de validação. O formulário deve ser preenchido corretamente"
                    ],
                    Response::HTTP_BAD_REQUEST
                );
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($secretaria);
            $em->flush();

            return $this->view(
                [
                    "status" => "OK",
                    "secretaria" => $secretaria
                ],
                Response::HTTP_OK
            );

        }
        catch (Exception $e)
        {
            \Monolog\Handler\error_log($e->getMessage());
            return $this->view(
                $this->errorResponse,
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    /**
     * @Rest\Put("/api/secretarias/{id}", name="put_secretarias")
     * @View(serializerGroups={"secretarias"})
     * @IsGranted("ROLE_GERENTE")
     */
    public function putSecretaria(Request $request, Secretaria $secretaria)
    {
        try
        {
            $form = $this->createForm(SecretariaType::class, $secretaria, [
                "csrf_protection" => false
            ]);

            $form->submit($request->request->all());
            if ($form->isValid() === false)
            {
                $formErrors = ControllerUtils::getFormErrors($form);
                return $this->view(
                    [
                        "status" => "VALIDATION_ERROR",
                        "errors" => $formErrors,
                        "message" => "Erro de validação. O formulário deve ser preenchido corretamente"
                    ],
                    Response::HTTP_BAD_REQUEST
                );
            }

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->view(
                [
                    "status" => "OK",
                    "secretaria" => $secretaria
                ],
                Response::HTTP_OK
            );
        }
        catch (Exception $e)
        {
            \Monolog\Handler\error_log($e->getMessage());
            return $this->view(
                $this->errorResponse,
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    /**
     * @Rest\Delete("/api/secretarias/{id}", name="delete_secretarias")
     * @IsGranted("ROLE_GERENTE")
     */
    public function deleteSecretaria(Secretaria $secretaria)
    {
        try
        {
            $em = $this->getDoctrine()->getManager();
            $em->remove($secretaria);

            $em->flush();
            return $this->view(
                [
                    "status" => "OK"
                ],
                Response::HTTP_OK
            );
        }
        catch (Exception $e)
        {
            \Monolog\Handler\error_log($e->getMessage());
            return $this->view(
                $this->errorResponse,
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    /**
     * @Rest\Get("/api/secretarias/relatorio-salario", name="get_salario_secretarias")
     * @IsGranted("ROLE_GERENTE")
     */
    public function getSalarioSecretariasReport()
    {
        try {
            $em = $this->getDoctrine()->getManager();
            $result = $em->getRepository(Secretaria::class)->salarioSecretarias();

            return $this->view(
                [
                    "status" => "OK",
                    "salarioSecretariasReport" => $result
                ],
                Response::HTTP_OK
            );
        } catch (Exception $e) {
            \Monolog\Handler\error_log($e->getMessage());
            return $this->view(
                $this->errorResponse,
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }
}
