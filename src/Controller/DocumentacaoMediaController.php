<?php

namespace App\Controller;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\View;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

class DocumentacaoMediaController extends AbstractFOSRestController
{
    private const DIR = "/tmp/upload";
    private const MIME_TYPE_PREFIX = 'image/';
    private const CONTENT_NAME_HEADER = 'content-name';

    private $isValid = true;

    private static function getImageExtension($contentType) {
        return explode('/', $contentType)[1];
    }

    private function validateRequest($request) {
        $errors = [];
        $contentType = $request->headers->get('content-type');
        $isImage = substr($contentType, 0, strlen(self::MIME_TYPE_PREFIX)) === self::MIME_TYPE_PREFIX;
        if (false === $isImage) {
            $this->isValid = false;
            $errors[] = "A documentação deve ser uma imagem";
        }

        $hasContentName = $request->headers->has(self::CONTENT_NAME_HEADER);
        if (false === $hasContentName) {
            $this->isValid = false;
            $errors[] = "A documentação deve ter um nome";
        }

        return $errors;
    }

    /**
     * @Rest\Post("/api/media/documentacao", name="post_media_documentacao")
     * @View()
     * IsGranted("ROLE_OPERADOR")
     */
    public function postMediaDocumentacao(LoggerInterface $logger, Request $request)
    {
        try {
            $errors = $this->validateRequest($request);
            if (false === $this->isValid) {
                return $this->view(
                    [
                        "status" => "VALIDATION_ERROR",
                        "errors" => $errors,
                        "message" => "Erro de validação. A documentação deve ser enviada corretamente",
                    ],
                    Response::HTTP_BAD_REQUEST
                );
            }

            $contentType = $request->headers->get('content-type');
            $imageExtension = self::getImageExtension($contentType);

            $imageName = $request->headers->get(self::CONTENT_NAME_HEADER);
            $timestamp = date_format(new \DateTime(),'Y-m-dTH:i:s');

            $filename = md5($imageName.$timestamp.'.'.$imageExtension);
            $filenameFullpath = self::DIR.'/'.$filename;

            $fs = new Filesystem();
            $fs->dumpFile($filenameFullpath, $request->getContent());

            return $this->view(
                [
                    "status" => "OK",
                    "uuid" => $filename,
                ],
                Response::HTTP_CREATED
            );
        } catch (\Exception $e) {
            $logger->error($e->getMessage());
            return $this->view(
                $e->getMessage(),
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    /**
     * @Rest\Delete("/api/media/documentacao/{uuid}", name="delete_media_documentacao")
     * @View()
     * @IsGranted("ROLE_OPERADOR")
     */
    public function deleteMediaDocumentacao(LoggerInterface $logger, string $uuid) {
        try {
            $finder = new Finder();
            $finder->files();
            $finder->depth("== 0");

            $finder->name($uuid);
            $finder->in(self::DIR);

            if ($finder->count() < 1) {
                return $this->view(
                    [
                        "status" => "MEDIA_NOT_FOUND",
                        "message" => "A imagem requisitada não foi encontrada"
                    ],
                    Response::HTTP_NOT_FOUND
                );
            }

            $filesystem = new Filesystem();
            $fullFilename = self::DIR."/".$uuid;
            $filesystem->remove($fullFilename);

            return $this->view(
                [
                    "status" => "OK"
                ],
                Response::HTTP_OK
            );
        } catch (\Exception $e) {
            $logger->error($e->getMessage());
            return $this->view(
                $e->getMessage(),
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    /**
     * @Rest\Get("/api/media/documentacao/{uuid}", name="get_media_documentacao")
     * @IsGranted("ROLE_OPERADOR")
     */
    public function getMediaDocumentacao(SerializerInterface $serializer, LoggerInterface $logger, string $uuid) {
        $response = new Response();
        try {
            $finder = new Finder();
            $finder->files();

            $finder->depth("== 0");
            $finder->name($uuid);
            $finder->in(self::DIR);

            if ($finder->count() > 1) {
                throw new \Exception("Há mais de um arquivo no resultado da consulta.");
            }

            if ($finder->count() < 1) {
                $logger->info("Não achou documentacao. UUID: ${uuid}");
                $response->setStatusCode(Response::HTTP_NOT_FOUND);
                $jsonResponse = [
                    "status" => "MEDIA_NOT_FOUND",
                    "message" => "A imagem requisitada não foi encontrada"
                ];
                $response->headers->set("Content-Type", "application/json");
                $response->setContent($serializer->serialize($jsonResponse, 'json'));
                return $response;
            }

            $contents = null;
            $size = 0;
            foreach ($finder as $file) {
                $contents = $file->getContents();
                $size = $file->getSize();
            }

            $response->setStatusCode(Response::HTTP_OK);
            $response->headers->set("Content-Type", "image/*");
            $response->headers->set("Content-Length", $size);
            $response->setContent($contents);

            return $response;
        } catch (\Exception $e) {
            $logger->error($e->getMessage());
            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
            $jsonResponse = [
                "status" => "SERVER_ERROR",
                "message" => "Houve um erro no servidor. Tente novamente mais tarde."
            ];
            $response->headers->set("Content-Type", "application/json");
            $response->setContent($serializer->serialize($jsonResponse, 'json'));
            return $response;
        }
    }
}
