<?php

namespace App\Controller;

use App\Entity\Funcionario;
use App\Form\FuncionarioPeriodoAdmissaoExoneracaoType;
use App\Form\FuncionarioType;
use App\Utils\ControllerUtils;
use Doctrine\Common\Collections\ArrayCollection;
use Exception;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class FuncionarioController
 * @package App\Controller
 * @IsGranted("ROLE_OPERADOR")
 */
class FuncionarioController extends AbstractFOSRestController
{
    private $errorResponse = [
        "status" => "EXCEPTION_ERROR",
        "message" => "Erro no servidor. Contate o administrador do sistema."
    ];

    private $serializerContext;

    public function __construct()
    {
        $this->serializerContext = new Context();
        $this->serializerContext->setAttribute(DateTimeNormalizer::FORMAT_KEY, "Y-m-d");
    }

    /**
     * @Rest\Get("/api/funcionarios", name="funcionarios")
     * @View(serializerGroups={"funcionarios"})
     */
    public function funcionarios(LoggerInterface $logger)
    {
        try {
            $em = $this->getDoctrine()->getManager();
            $funcionarios = $em->getRepository(Funcionario::class)->findAll();

            $view = $this->view(
                [
                    "status" => "OK",
                    "funcionarios" => $funcionarios,
                ],
                Response::HTTP_OK
            );

            $view->setContext($this->serializerContext);
            return $view;
        } catch (Exception $e) {
            $logger->error($e->getMessage());
            return $this->view(
                $this->errorResponse,
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    /**
     * @Rest\Get("/api/funcionarios/relatorio-admissao-exoneracao", name="get_funcionario_periodo_admissao_exoneracao_report")
     * @QueryParam(name="dateBegin", nullable=false, requirements=@Assert\DateTime(format="Y-m-d"))
     * @QueryParam(name="dateEnd", nullable=false, requirements=@Assert\DateTime(format="Y-m-d"))
     * @QueryParam(name="isAdmissao", nullable=false, requirements=@Assert\Choice({"true", "false"}))
     * @View(serializerGroups={"funcionarios"})
     */
    public function funcionarioPeriodoAdmissaoExoneracaoReport(ParamFetcherInterface $paramFetcher, LoggerInterface $logger)
    {
        try {
            $formData = [];
            $requestData = $paramFetcher->all();

            $form = $this->createForm(FuncionarioPeriodoAdmissaoExoneracaoType::class, $formData, [
                "csrf_protection" => false,
            ]);

            $form->submit($requestData);
            if ($form->isValid() === false) {
                $formErrors = ControllerUtils::getFormErrors($form);
                return $this->view(
                    [
                        "status" => "VALIDATION_ERROR",
                        "errors" => $formErrors,
                        "message" => "Erro de validação. O formulário deve ser preenchido corretamente"
                    ],
                    Response::HTTP_BAD_REQUEST
                );
            }

            $dateBegin = $form->getData()['dateBegin'];
            $dateEnd = $form->getData()['dateEnd'];
            $isAdmissao = $form->getData()['isAdmissao'] === 'true' ? true : false;

            $em = $this->getDoctrine()->getManager();
            $report = $em->getRepository(Funcionario::class)->funcionariosPeriodoAdmissaoExoneracao(
                $dateBegin, $dateEnd, $isAdmissao
            );

            $view = $this->view(
                [
                    "status" => "OK",
                    "funcionariosPeriodoAdmissaoExoneracao" => $report
                ],
                Response::HTTP_OK
            );

            $view->setContext($this->serializerContext);
            return $view;
        } catch (Exception $e) {
            $logger->error($e->getMessage());
            return $this->view(
                $this->errorResponse,
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    /**
     * @Rest\Get("/api/funcionarios/{id}", name="get_funcionario")
     * @View(serializerGroups={"funcionarios"})
     */
    public function getFuncionario(Funcionario $funcionario, LoggerInterface $logger)
    {
        try {
            $view = $this->view(
                [
                    "status" => "OK",
                    "funcionario" => $funcionario
                ],
                Response::HTTP_OK
            );

            $view->setContext($this->serializerContext);
            return $view;
        }
        catch (Exception $e) {
            $logger->error($e->getMessage());
            return $this->view(
                $this->errorResponse,
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    /**
     * @Rest\Post("/api/funcionarios", name="post_funcionario")
     * @View(serializerGroups={"funcionarios"})
     */
    public function postFuncionario(Request $request, LoggerInterface $logger)
    {
        try {
            $funcionario = new Funcionario();
            $form = $this->createForm(FuncionarioType::class, $funcionario, [
                "csrf_protection" => false
            ]);

            $form->submit($request->request->all());
            //return $this->json($form->getErrors(true));
            if ($form->isValid() === false) {
                $formErrors = ControllerUtils::getFormErrors($form);
                return $this->view(
                    [
                        "status" => "VALIDATION_ERROR",
                        "errors" => $formErrors,
                        "message" => "Erro de validação. O formulário deve ser preenchido corretamente"
                    ],
                    Response::HTTP_BAD_REQUEST
                );
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($funcionario);
            $em->flush();

            $view = $this->view(
                [
                    "status" => "OK",
                    "funcionario" => $funcionario
                ],
                Response::HTTP_CREATED
            );

            $view->setContext($this->serializerContext);
            return $view;
        }
        catch (Exception $e) {
            $logger->error($e->getMessage());
            return $this->view(
                $e->getMessage(),
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    /**
     * @Rest\Put("/api/funcionarios/{id}", name="put_funcionario")
     * @View(serializerGroups={"funcionarios"})
     */
    public function putFuncionario(Request $request, LoggerInterface $logger, Funcionario $funcionario)
    {
        try {

            $originalDocumentationMedias = new ArrayCollection();
            foreach ($funcionario->getDocumentationMedias() as $documentationMedia) {
                $originalDocumentationMedias->add($documentationMedia);
            }

            $form = $this->createForm(FuncionarioType::class, $funcionario, [
                "csrf_protection" => false
            ]);
            $form->submit($request->request->all());

            if ($form->isValid() === false) {
                $formErrors = ControllerUtils::getFormErrors($form);
                return $this->view(
                    [
                        "status" => "VALIDATION_ERROR",
                        "errors" => $formErrors,
                        "message" => "Erro de validação. O formulário deve ser preenchido corretamente"
                    ],
                    Response::HTTP_BAD_REQUEST
                );
            }

            $em = $this->getDoctrine()->getManager();

            foreach ($originalDocumentationMedias as $originalDocumentationMedia) {
                if (false === $funcionario->getDocumentationMedias()->contains($originalDocumentationMedia)) {
                    $originalDocumentationMedia->setFuncionario(null);
                    $em->remove($originalDocumentationMedia);
                } else {
                    $originalDocumentationMedia->setFuncionario($funcionario);
                }
            }

            $em->flush();

            $view = $this->view(
                [
                    "status" => "OK",
                    "funcionario" => $funcionario,
                ],
                Response::HTTP_OK
            );

            $view->setContext($this->serializerContext);
            return $view;
        }
        catch (Exception $e) {
            $logger->error($e->getMessage());
            return $this->view(
                $this->errorResponse,
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    /**
     * @Rest\Delete("/api/funcionarios/{id}", name="delete_funcionario")
     */
    public function deleteFuncionario(Funcionario $funcionario, LoggerInterface $logger)
    {
        try {
            $em = $this->getDoctrine()->getManager();
            $em->remove($funcionario);

            $em->flush();

            return $this->view(
                [
                    "status" => "OK"
                ],
                Response::HTTP_OK
            );
        }
        catch (Exception $e) {
            $logger->error($e->getMessage());
            return $this->view(
                $this->errorResponse,
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }
}
