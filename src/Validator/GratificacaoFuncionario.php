<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;
use App\Entity\Posse;

/**
 * @Annotation
 */
class GratificacaoFuncionario extends Constraint
{
    /*
     * Any public properties become valid options for the annotation.
     * Then, use these in your validator class.
     */
    public $message = "Funcionário comissionado não pode ter gratificação";
}
