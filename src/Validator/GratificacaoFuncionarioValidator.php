<?php

namespace App\Validator;

use Symfony\Component\PropertyAccess\Exception\NoSuchPropertyException;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use App\Entity\Posse;
use Symfony\Component\Validator\Exception\ConstraintDefinitionException;

class GratificacaoFuncionarioValidator extends ConstraintValidator
{
    private $propertyAccessor;

    public function __construct(PropertyAccessorInterface $propertyAccessor = null)
    {
        $this->propertyAccessor = $propertyAccessor;
    }

    public function validate($value, Constraint $constraint)
    {
        if (null === $value)
        {
            return;
        }

        if (null === $object = $this->context->getObject())
        {
            return;
        }
        try
        {
            $path = "posse.tipo";
            $posseTipo = $this->getPropertyAccessor()->getValue($object, $path);

            /* @var $constraint App\Validator\GratificacaoFuncionario */
            if ($posseTipo === Posse::COMISSIONADO && $value > 0.0)
            {
                $this->context->buildViolation($constraint->message)
                    ->addViolation();
            }
        }
        catch (NoSuchPropertyException $e)
        {
            throw new ConstraintDefinitionException(sprintf('Invalid property path "%s" provided to "%s" constraint: %s', $path, \get_class($constraint), $e->getMessage()), 0, $e);
        }

    }

    /**
     * @return PropertyAccessorInterface|null
     */
    public function getPropertyAccessor(): ?PropertyAccessorInterface
    {
        return $this->propertyAccessor;
    }

    /**
     * @param PropertyAccessorInterface|null $propertyAccessor
     * @return GratificacaoFuncionarioValidator
     */
    public function setPropertyAccessor(PropertyAccessorInterface $propertyAccessor): GratificacaoFuncionarioValidator
    {
        $this->propertyAccessor = $propertyAccessor;
        return $this;
    }
}
