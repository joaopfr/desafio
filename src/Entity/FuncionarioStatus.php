<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FuncionarioStatusRepository")
 */
class FuncionarioStatus
{
    const ATIVO = "ATIVO";
    const EXONERADO = "EXONERADO";
    const STATUS = [
        self::ATIVO,
        self::EXONERADO
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var mixed
     * @ORM\Column(type="string")
     * @Assert\Choice(choices=FuncionarioStatus::STATUS)
     */
    private $status;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     * @Groups({"funcionarios"})
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return FuncionarioStatus
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }
}
