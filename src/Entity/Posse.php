<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PosseRepository")
 */
class Posse
{
    const ESTATUTARIO = "ESTATUTARIO";
    const COMISSIONADO = "COMISSIONADO";

    const TIPOS = [
        self::ESTATUTARIO,
        self::COMISSIONADO
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var mixed
     * @ORM\Column(type="string")
     * @Assert\Choice(Posse::TIPOS)
     */
    private $tipo;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     * @Groups({"funcionarios"})
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * @param mixed $tipo
     * @return Posse
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
        return $this;
    }
}
