<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Validator as DesafioAssert;
use Symfony\Component\Validator\GroupSequenceProviderInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FuncionarioRepository")
 * @Assert\GroupSequenceProvider()
 */
class Funcionario implements GroupSequenceProviderInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    private $nome;

    /**
     * @ORM\Column(type="string", length=11)
     * @Assert\NotBlank
     */
    private $cpf;

    /**
     * @var mixed
     * @ORM\ManyToOne(targetEntity="App\Entity\Secretaria", inversedBy="secretarias")
     * @Assert\NotBlank
     */
    private $secretaria;

    /**
     * @var mixed
     * @ORM\ManyToOne(targetEntity="App\Entity\FuncionarioStatus")
     * @Assert\NotBlank
     */
    private $status;

    /**
     * @var mixed
     * @ORM\ManyToOne(targetEntity="App\Entity\Posse")
     * @Assert\NotBlank
     */
    private $posse;

    /**
     * @var mixed
     * @ORM\Column(type="decimal", precision=8, scale=2, options={"default": 0})
     * @Assert\NotBlank
     * @Assert\GreaterThanOrEqual(0)
     */
    private $salarioBase;

    /**
     * @var mixed
     * @ORM\Column(type="decimal", precision=8, scale=2, options={"default": 0})
     * @Assert\NotBlank
     * @Assert\GreaterThanOrEqual(0)
     * @DesafioAssert\GratificacaoFuncionario
     */
    private $gratificacao;

    /**
     * @var mixed
     * @ORM\Column(type="decimal", precision=8, scale=2, options={"default": 0})
     * @Assert\NotBlank
     * @Assert\GreaterThanOrEqual(0)
     */
    private $desconto;

    /**
     * @var mixed
     * @ORM\Column(type="date", nullable=true)
     * @Assert\NotBlank
     */
    private $dataAdmissao;

    /**
     * @var mixed
     * @ORM\Column(type="date", nullable=true)
     * @Assert\NotBlank(groups={"exonerado"})
     * @Assert\GreaterThan(propertyPath="dataAdmissao", groups={"exonerado"})
     */
    private $dataExoneracao;

    /**
     * @var mixed
     * @ORM\OneToMany(targetEntity="App\Entity\DocumentationMedia", mappedBy="funcionario", cascade={"persist", "remove"})
     */
    private $documentationMedias;

    public function __construct()
    {
        $this->documentationMedias = new ArrayCollection();
    }

    /**
     * @Groups({"funcionarios"})
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     * @Groups({"funcionarios"})
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     * @return Funcionario
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    /**
     * @return mixed
     * @Groups({"funcionarios"})
     */
    public function getCpf()
    {
        return $this->cpf;
    }

    /**
     * @param mixed $cpf
     * @return Funcionario
     */
    public function setCpf($cpf)
    {
        $this->cpf = $cpf;
        return $this;
    }

    /**
     * @return mixed
     * @Groups({"funcionarios"})
     */
    public function getSecretaria()
    {
        return $this->secretaria;
    }

    /**
     * @param mixed $secretaria
     * @return Funcionario
     */
    public function setSecretaria($secretaria)
    {
        $this->secretaria = $secretaria;
        return $this;
    }

    /**
     * @return mixed
     * @Groups({"funcionarios"})
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return Funcionario
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     * @Groups({"funcionarios"})
     */
    public function getPosse()
    {
        return $this->posse;
    }

    /**
     * @param mixed $posse
     * @return Funcionario
     */
    public function setPosse($posse)
    {
        $this->posse = $posse;
        return $this;
    }

    /**
     * @return mixed
     * @Groups({"funcionarios"})
     */
    public function getSalarioBase()
    {
        return $this->salarioBase;
    }

    /**
     * @param mixed $salarioBase
     * @return Funcionario
     */
    public function setSalarioBase($salarioBase)
    {
        $this->salarioBase = is_null($salarioBase) ? 0 : $salarioBase;
        return $this;
    }

    /**
     * @return mixed
     * @Groups({"funcionarios"})
     */
    public function getGratificacao()
    {
        return $this->gratificacao;
    }

    /**
     * @param mixed $gratificacao
     * @return Funcionario
     */
    public function setGratificacao($gratificacao)
    {
        $this->gratificacao = is_null($gratificacao) ? 0 : $gratificacao;
        return $this;
    }

    /**
     * @return mixed
     * @Groups({"funcionarios"})
     */
    public function getDesconto()
    {
        return $this->desconto;
    }

    /**
     * @param mixed $desconto
     * @return Funcionario
     */
    public function setDesconto($desconto)
    {
        $this->desconto = is_null($desconto) ? 0 : $desconto;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSalarioLiquido()
    {
        return $this->getSalarioBase() + $this->getGratificacao() - $this->getDesconto();
    }

    /**
     * @return mixed
     * @Groups({"funcionarios"})
     */
    public function getDataAdmissao()
    {
        return $this->dataAdmissao;
    }

    /**
     * @param mixed $dataAdmissao
     * @return Funcionario
     */
    public function setDataAdmissao($dataAdmissao)
    {
        $this->dataAdmissao = $dataAdmissao;
        return $this;
    }

    /**
     * @return mixed
     * @Groups({"funcionarios"})
     */
    public function getDataExoneracao()
    {
        return $this->dataExoneracao;
    }

    /**
     * @param mixed $dataExoneracao
     * @return Funcionario
     */
    public function setDataExoneracao($dataExoneracao)
    {
        $this->dataExoneracao = $dataExoneracao;
        return $this;
    }

    /**
     * @return mixed
     * @Groups({"funcionarios"})
     */
    public function getDocumentationMedias()
    {
        return $this->documentationMedias;
    }

    /**
     * @param mixed $documentationMedias
     * @return Funcionario
     */
    public function setDocumentationMedias($documentationMedias)
    {
        $this->documentationMedias = $documentationMedias;
        return $this;
    }

    public function addDocumentationMedia(DocumentationMedia $documentationMedia) {
        if (!$this->documentationMedias->contains($documentationMedia)) {
            $this->documentationMedias[] = $documentationMedia;
            $documentationMedia->setFuncionario($this);
        }

        return $this;
    }

    public function removeDocumentationMedia(DocumentationMedia $documentationMedia) {
        if ($this->documentationMedias->contains($documentationMedia)) {
            $this->documentationMedias->removeElement($documentationMedia);

            if ($documentationMedia->getFuncionario() === $this) {
                $documentationMedia->setFuncionario(null);
            }
        }

        return $this;
    }

    public function getGroupSequence()
    {
        $groupSequence = ["Funcionario"];
        if ($this->getStatus()->getStatus() === FuncionarioStatus::EXONERADO) {
            $groupSequence[] = "exonerado";
        }

        return $groupSequence;
    }
}
