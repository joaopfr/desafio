<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DocumentationMediaRepository")
 */
class DocumentationMedia
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var mixed
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    private $uuid;

    /**
     * @var mixed
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    private $name;

    /**
     * @var mixed
     * @ORM\ManyToOne(targetEntity="App\Entity\Funcionario", inversedBy="documentationMedias")
     * @Assert\NotNull
     */
    private $funcionario;

    /**
     * @var mixed
     * @ORM\Column(type="string")
     * @Assert\NotNull
     */
    private $type;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     * @Groups({"funcionarios"})
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param mixed $uuid
     * @return DocumentationMedia
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
        return $this;
    }

    /**
     * @return mixed
     * @Groups({"funcionarios"})
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return DocumentationMedia
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFuncionario()
    {
        return $this->funcionario;
    }

    /**
     * @param mixed $funcionario
     * @return DocumentationMedia
     */
    public function setFuncionario($funcionario)
    {
        $this->funcionario = $funcionario;
        return $this;
    }

    /**
     * @return mixed
     * @Groups({"funcionarios"})
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return DocumentationMedia
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }
}
