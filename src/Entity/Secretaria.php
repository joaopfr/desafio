<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SecretariaRepository")
 */
class Secretaria
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var mixed
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    private $nome;

    /**
     * @var mixed
     * @ORM\OneToMany(targetEntity="App\Entity\Funcionario", mappedBy="secretaria")
     */
    private $funcionarios;

    /**
     * @Groups({"secretarias", "funcionarios", "secretariasSelector"})
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     * @Groups({"secretarias", "secretariasSelector"})
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     * @return Secretaria
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFuncionarios()
    {
        return $this->funcionarios;
    }

    /**
     * @param mixed $funcionarios
     * @return Secretaria
     */
    public function setFuncionarios($funcionarios)
    {
        $this->funcionarios = $funcionarios;
        return $this;
    }
}
